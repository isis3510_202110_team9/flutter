import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/styles/custom_theme.dart';

class ProfileCard extends StatefulWidget {
  final Widget icon;
  final Widget child;
  final String title;

  ProfileCard({
    Key key,
    this.title,
    this.icon,
    this.child,
  });

  @override
  _ProfileCardState createState() => _ProfileCardState();
}

class _ProfileCardState extends State<ProfileCard> {
  String title;
  Widget child;
  Image icon;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Card(
        color: CustomTheme.backGroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Container(
                    child: widget.icon,
                  ),
                  Container(
                    width: 20,
                  ),
                  Text(widget.title,
                      textScaleFactor: 1.2,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black)),
                ],
              ),
              widget.child,
            ],
          ),
        ),
      ),
    );
  }
}
