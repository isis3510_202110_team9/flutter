import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget buildTextInput(
    {bool obscure,
    String Function(String) validator,
    TextEditingController controller,
    String hint,
    String label,
    IconData icon,
    bool floatingLabel = false,
    BuildContext context,
    void Function(String) onChanged}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 8.0),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        !floatingLabel
            ? Text(
                label,
                style: Theme.of(context).inputDecorationTheme.labelStyle,
              )
            : SizedBox(
                width: 0,
                height: 0,
              ),
        TextFormField(
          style: TextStyle(fontWeight: FontWeight.w500),
          controller: controller,
          obscureText: obscure,
          onChanged: onChanged,
          decoration: InputDecoration(
            hintText: hint,
            labelText: floatingLabel ? label : null,
            prefixIcon: icon == null
                ? null
                : Icon(
                    icon,
                    size: 20,
                  ),
          ),
          validator: validator,
        ),
      ],
    ),
  );
}
