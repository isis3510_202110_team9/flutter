import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/styles/custom_theme.dart';

class MeditationCard extends StatefulWidget {
  final String title;
  final Function onTap;
  final bool disabled;
  final bool isLarge;

  MeditationCard({
    Key key,
    this.title,
    this.onTap,
    this.disabled,
    this.isLarge,
  });

  @override
  _MeditationCardState createState() => _MeditationCardState();
}

var assets = {
  "Anxiety": {"image": "anxiety.png", "time": "6 min"},
  "Attention": {"image": "attention.png", "time": "5 min"},
  "Balance": {"image": "candle.png", "time": "5 min"},
  "Breathing": {"image": "Breathing.png", "time": "5 min"},
  "Concentration": {"image": "concentration.png", "time": "6 min"},
  "Happiness": {"image": "happiness.png", "time": "4 min"},
  "Loading...": {"image": "happiness.png", "time": "4 min"},
  "anxiety": {"image": "anxiety.png", "time": "6 min"},
  "attention": {"image": "attention.png", "time": "5 min"},
  "balance": {"image": "candle.png", "time": "5 min"},
  "breathing": {"image": "Breathing.png", "time": "5 min"},
  "concentration": {"image": "concentration.png", "time": "6 min"},
  "happiness": {"image": "happiness.png", "time": "4 min"},
};

class _MeditationCardState extends State<MeditationCard> {
  String title;
  bool disabled;
  bool isLarge;

  @override
  void initState() {
    print(this.disabled);
    super.initState();
  }

  String capitalize(string) {
    if (string == null) {
      return '';
    }
    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    Widget content = Padding(
      padding: const EdgeInsets.all(5.0),
      child: GestureDetector(
        child: Card(
          color: Color(widget.disabled ? 0xffd5b3e6 : 0xffe4e2e8),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    capitalize(widget.title),
                    textScaleFactor: 1.2,
                    style: TextStyle(
                        fontSize: widget.isLarge ? 18 : 13,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Poppins',
                        color: widget.isLarge
                            ? Colors.black
                            : widget.disabled
                                ? CustomTheme.graduaDarkPurple
                                : Colors.black),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (widget.isLarge)
                          Container(
                            width: 250,
                            child: Text(
                              "It is a long established fact that a reader will be distracted by the readable content",
                              style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontSize: 13,
                                  color: widget.disabled
                                      ? CustomTheme.graduaDarkPurple
                                      : Colors.black),
                            ),
                          ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: EdgeInsets.only(top: 8.0),
                            child: Text("3 min",
                                style: TextStyle(color: Colors.black45),
                                textAlign: TextAlign.left),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 10.0,
                      ),
                      child: Container(
                        height: 45.0,
                        child: Image.asset("assets/images/" +
                            (assets[widget.title] != null
                                ? assets[widget.title]["image"]
                                : "candle.png")),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        onTap: widget.onTap,
      ),
    );

    if (!widget.isLarge) {
      return Container(
          height: 130, width: 250, color: Colors.transparent, child: content);
    }

    return content;
  }
}
