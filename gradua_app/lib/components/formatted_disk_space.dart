import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle yellow() {
  return TextStyle(
    fontFamily: 'Poppins',
    color: Colors.amber,
    fontWeight: FontWeight.w500,
    fontSize: 11,
  );
}

TextStyle red() {
  return TextStyle(
    fontFamily: 'Poppins',
    color: Colors.redAccent,
    fontWeight: FontWeight.w500,
    fontSize: 11,
  );
}

TextStyle green() {
  return TextStyle(
    fontFamily: 'Poppins',
    color: Colors.green,
    fontWeight: FontWeight.w500,
    fontSize: 11,
  );
}

Text formattedDiskSpace(double space) {
  TextStyle style = green();
  String medida = 'MB';
  double medicion = space;

  if (medicion / 1024 < 10) {
    style = yellow();
  } else if (medicion / 1024 < 3) {
    style = red();
  }

  if (space < 1) {
    medicion *= 1024;
    medida = 'KB';
    style = green();
  } else if (space > 500) {
    medicion /= 1024;
    medida = 'GB';
  }
  medicion = double.parse(medicion.toStringAsFixed(2));
  String text = "Avlb Storage: ${medicion} ${medida}";
  // print('medición: ${text}');
  return Text(
    text,
    style: style,
  );
}

Text formattedFileSize(double size) {
  TextStyle style = null;
  String medida = 'MB';
  double medicion = size;

  if (size < 1) {
    medicion *= 1024;
    medida = 'KB';
    style = green();
  } else if (size < 3) {
    style = yellow();
  } else if (size > 500) {
    style = red();
    medicion /= 1024;
    medida = 'GB';
  } else {
    style = red();
  }

  medicion = double.parse(medicion.toStringAsFixed(2));
  String text = "${medicion} ${medida}";
  // print('medición: ${text}');
  return Text(
    text,
    style: style,
  );
}
