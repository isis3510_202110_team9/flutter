import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget playButton(void Function() onPressed) {
  return OutlinedButton(
    key: Key('play_button'),
    onPressed: onPressed,
    style: ButtonStyle(
      padding: MaterialStateProperty.all<EdgeInsets>(
        EdgeInsets.all(15),
      ),
      side: MaterialStateProperty.all<BorderSide>(
        BorderSide(
          width: 2,
          color: Color.fromRGBO(137, 144, 154, 1),
        ),
      ),
      shape: MaterialStateProperty.all<OutlinedBorder>(CircleBorder()),
    ),
    child: Icon(CupertinoIcons.play_fill,
        color: Color.fromRGBO(137, 144, 154, 1), size: 24.0),
  );
}

Widget pauseButton(void Function() onPressed) {
  return OutlinedButton(
    key: Key('pause_button'),
    onPressed: onPressed,
    style: ButtonStyle(
      padding: MaterialStateProperty.all<EdgeInsets>(
        EdgeInsets.all(15),
      ),
      side: MaterialStateProperty.all<BorderSide>(
        BorderSide(
          width: 2,
          color: Color.fromRGBO(137, 144, 154, 1),
        ),
      ),
      shape: MaterialStateProperty.all<OutlinedBorder>(
        CircleBorder(),
      ),
    ),
    child: Icon(
      CupertinoIcons.pause,
      size: 24.0,
      color: Color.fromRGBO(137, 144, 154, 1),
    ),
  );
}

Widget OutlinedHeartButton(void Function() onPressed) {
  return TextButton(
    key: Key('heart_button_outlined'),
    onPressed: onPressed,
    child: Padding(
      padding: const EdgeInsets.fromLTRB(35, 10, 0, 0),
      child: Icon(
        CupertinoIcons.heart,
        size: 30,
        color: Color.fromRGBO(137, 144, 154, 1),
      ),
    ),
  );
}

Widget FilledHeartButton(void Function() onPressed) {
  return TextButton(
    key: Key('heart_button_outlined'),
    onPressed: onPressed,
    child: Padding(
      padding: const EdgeInsets.fromLTRB(35, 10, 0, 0),
      child: Icon(
        CupertinoIcons.heart_solid,
        size: 30,
        color: Colors.white,
      ),
    ),
  );
}

Widget downloadButton(void Function() onPressed) {
  return TextButton(
    key: Key('filled_down_arrow_button'),
    onPressed: onPressed,
    child: Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: Icon(
        CupertinoIcons.arrow_down_circle_fill,
        size: 30,
        color: Colors.blueGrey,
      ),
    ),
  );
}

Widget deleteDownloadedButton(Future<Function> onPressed) {
  return TextButton(
    key: Key('filled_down_arrow_button'),
    onPressed: () async {
      Function func = await onPressed;
      func();
    },
    child: Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: Icon(
        CupertinoIcons.delete_simple,
        size: 25,
        color: Colors.redAccent,
      ),
    ),
  );
}
