import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

SnackBar ConnectionLostAlert() {
  return SnackBar(
    content: Container(
      child: Wrap(
        direction: Axis.horizontal,
        alignment: WrapAlignment.center,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          Icon(
            CupertinoIcons.info,
            size: 30,
            color: Colors.white,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              children: [
                Text(
                  "Internet connection lost",
                  style: TextStyle(
                    fontSize: 16,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  "Please check your internet settings",
                  style: TextStyle(
                    fontSize: 10,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
    duration: Duration(seconds: 5),
    behavior: SnackBarBehavior.floating,
    shape: StadiumBorder(),
    padding: EdgeInsets.symmetric(vertical: 4),
    backgroundColor: Color.fromRGBO(0, 0, 0, 0.8),
  );
}

SnackBar ConnectionRestoredAlert() => SnackBar(
      content: Container(
        child: Wrap(
          direction: Axis.horizontal,
          alignment: WrapAlignment.center,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Icon(
              CupertinoIcons.info,
              size: 30,
              color: Colors.white,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.center,
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Connection Restored",
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      duration: Duration(seconds: 3),
      behavior: SnackBarBehavior.floating,
      shape: StadiumBorder(),
      padding: EdgeInsets.symmetric(vertical: 4),
      backgroundColor: Color.fromRGBO(0, 0, 0, 0.8),
    );

CupertinoAlertDialog authNoConnectionAlert(BuildContext context) {
  return CupertinoAlertDialog(
    title: Text("There is no internet connection"),
    content: Text(
        "You can't authenticate without connection.\n Check your internet settings"),
    actions: [
      TextButton(
        onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
        child: Text(
          "OK",
          style: TextStyle(color: Colors.blue),
        ),
      )
    ],
  );
}

CupertinoAlertDialog termsAndServicesAlert(BuildContext context) {
  return CupertinoAlertDialog(
    title: Text("You must accept the terms of services to continue."),
    content: Text(
        "You can not authenticate without accepting term of services and Privacy Policies"),
    actions: [
      TextButton(
        onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
        child: Text(
          "OK",
          style: TextStyle(color: Colors.blue),
        ),
      )
    ],
  );
}

CupertinoAlertDialog quickMeditationAlert(BuildContext context) {
  return CupertinoAlertDialog(
    title: Text("You need internet connection to get a Quick Meditation."),
    content: Text("Please check your internet connection settings."),
    actions: [
      TextButton(
        onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
        child: Text(
          "OK",
          style: TextStyle(color: Colors.blue),
        ),
      )
    ],
  );
}

CupertinoAlertDialog badSignInAlert(BuildContext context) {
  return CupertinoAlertDialog(
    title: Text("There was an error sign in in"),
    content: Text("Check the entry fields or change the email"),
    actions: [
      TextButton(
        onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
        child: Text(
          "OK",
          style: TextStyle(color: Colors.blue),
        ),
      )
    ],
  );
}

CupertinoAlertDialog badSignUpAlert(BuildContext context) {
  return CupertinoAlertDialog(
    title: Text("There was an error sign in up."),
    content: Text("Check the entry fields or change the email"),
    actions: [
      TextButton(
        onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
        child: Text(
          "OK",
          style: TextStyle(color: Colors.blue),
        ),
      )
    ],
  );
}

CupertinoAlertDialog playerPlayAlert(BuildContext context) {
  return CupertinoAlertDialog(
    title: Text("You need internet connection to reproduce this meditation"),
    content: Text("Please check your internet connection settings."),
    actions: [
      TextButton(
        onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
        child: Text(
          "OK",
          style: TextStyle(color: Colors.blue),
        ),
      )
    ],
  );
}

CupertinoAlertDialog playerDeletAlert(
    BuildContext context, Function deletionCallback) {
  return CupertinoAlertDialog(
    title: Text("Are you sure you want to delete this file?"),
    content: Text(
        "With no internet, you won't be able to listen to this meditation"),
    actions: [
      TextButton(
        onPressed: () {
          deletionCallback();
          Navigator.of(context, rootNavigator: true)
              .popUntil((route) => route.isFirst);
        },
        child: Text(
          "OK",
          style: TextStyle(color: Colors.blue),
        ),
      ),
      TextButton(
        onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
        child: Text(
          "Cancel",
          style: TextStyle(color: Colors.blue),
        ),
      ),
    ],
  );
}
