import "dart:async";

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:noise_meter/noise_meter.dart';

class AmbientNoiseWidget extends StatefulWidget {
  @override
  _AmbientNoiseWidgetState createState() => _AmbientNoiseWidgetState();
}

class _AmbientNoiseWidgetState extends State<AmbientNoiseWidget> {
  bool _isRecording = false;
  String noiseAlert = 'Alerta';
  StreamSubscription<NoiseReading> _noiseSubscription;
  NoiseMeter _noiseMeter;
  var meanDecibel;

  @override
  void initState() {
    super.initState();
    _noiseMeter = new NoiseMeter(onError);
  }

  void onData(NoiseReading noiseReading) {
    this.setState(() {
      if (!this._isRecording) {
        this._isRecording = true;
      }
      meanDecibel = noiseReading.meanDecibel.roundToDouble();
    });
    print(noiseReading.meanDecibel);
  }

  void onError(PlatformException e) {
    print(e.toString());
    _isRecording = false;
  }

  void start() async {
    try {
      _noiseSubscription = _noiseMeter.noiseStream.listen(onData);
      // _measuringNoise = true;
      Future.delayed(Duration(seconds: 15)).then((value) => stop());
    } catch (err) {
      print('Errors on noise subscription');
      print(err);
    }
  }

  void stop() async {
    try {
      if (_noiseSubscription != null) {
        _noiseSubscription.cancel();
        _noiseSubscription = null;
      }
      this.setState(() {
        if (!_isRecording) {
          noiseAlert = 'Noisy environment';
        } else {
          noiseAlert = 'Good environment';
        }
        this._isRecording = false;
      });
    } catch (err) {
      print('stopRecorder error: $err');
    }
  }

  TextStyle getMeasurementStyle(double measure) {
    if (measure != null && measure > 85) {
      return TextStyle(
        fontSize: 11,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
        color: Colors.redAccent,
      );
    } else {
      return TextStyle(
        fontSize: 11,
        fontFamily: 'Poppins',
        fontWeight: FontWeight.w600,
        color: Colors.green,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Container(
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
              child: Visibility(
                visible: !_isRecording,
                child: IconButton(
                  onPressed: start,
                  icon: Icon(
                    CupertinoIcons.mic_fill,
                    color: Color.fromRGBO(137, 144, 154, 1),
                  ),
                ),
                replacement: Padding(
                  padding: EdgeInsets.only(top: 15),
                  child: Text(
                    '$meanDecibel db',
                    style: getMeasurementStyle(meanDecibel),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
