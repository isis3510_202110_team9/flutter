import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class DbManager {
  // -------
  // CONSTS
  // -------

  /* Allows this class be a singleton */
  static final DbManager _dbManager = DbManager._internal();

  /* Users collection reference */
  final CollectionReference _usersCol =
      FirebaseFirestore.instance.collection("users");

  /* Users collection reference */
  final CollectionReference _meditationsCol =
      FirebaseFirestore.instance.collection("meditations");

  // --------
  // METHODS
  // --------
  factory DbManager() {
    return _dbManager;
  }

  DbManager._internal();

  Future createUser(uid, user) async {
    return await _usersCol.doc(uid).set(user.getFullName());
  }

  Future persistInterests(uid, interests) async {
    return await _usersCol.doc(uid).update({'interests': interests});
  }

  Future incrementMeditationReproductions(meditationId) async {
    var meditation = await _meditationsCol.doc(meditationId).get();

    int currentReproductionsValue = meditation.get("reproductions");

    return await _meditationsCol
        .doc(meditationId)
        .update({'reproductions': currentReproductionsValue + 1});
  }

  getPopularMeditations() async {
    QuerySnapshot querySnapshot = await _meditationsCol
        .orderBy("reproductions", descending: true)
        .limit(3)
        .get();

    var response = [];

    querySnapshot.docs.forEach((doc) {
      response.add(doc.data());
    });

    return (response);
  }
}
