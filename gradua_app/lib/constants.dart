import 'package:flutter/cupertino.dart';

const graduaPurple = Color(0xFF7400B8);
const graduaPrupleBlue = Color(0xFF5E60CE);
const graduaDarkBlue = Color(0xFF4EA8DE);
const graduaLightBlue = Color(0xFF48BFE3);
const graduaMagenta = Color(0xFF80FFDB);
