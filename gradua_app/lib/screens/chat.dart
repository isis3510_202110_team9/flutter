import 'package:flutter/material.dart';
import 'package:gradua_app/screens/chat_bot.dart';
import 'package:gradua_app/styles/gradients.dart';

class Chat extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<Chat> {
  @override
  Widget build(BuildContext context) {
    final dividerWidth = 80;
    final dividerIndent =
        MediaQuery.of(context).size.width / 2 - dividerWidth / 2;
    return Container(
      color: Theme.of(context).canvasColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Divider(
            indent: dividerIndent,
            endIndent: dividerIndent,
            thickness: 4,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 20, 30, 15),
            child: Text(
              "Hi, I am Gradua",
              style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.w500,
                fontFamily: "Roboto",
              ),
            ),
          ),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Chatbot(),
          )),
        ],
      ),
    );
  }
}
