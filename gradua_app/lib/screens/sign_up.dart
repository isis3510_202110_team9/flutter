import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/components/connection_alert.dart';
import 'package:gradua_app/components/text_input.dart';
import 'package:gradua_app/layout/scroll_sheet.dart';
import 'package:gradua_app/services/authentication_service.dart';
import 'package:gradua_app/services/connection_status_service.dart';
import 'package:gradua_app/services/db_manager.dart';
import 'package:gradua_app/services/navigation_service.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:gradua_app/view_model/user_view_model.dart';
import 'package:provider/provider.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  UserAuthViewModel inputController = UserAuthViewModel();

  final double _titleSize = 40.0;

  final double _subtitleSize = 17.0;

  final _formKey = GlobalKey<FormState>();

  TextStyle _getTextStyle({double size}) {
    return TextStyle(
        fontFamily: "Roboto",
        fontWeight: FontWeight.bold,
        fontSize: size,
        height: 0.9);
  }

  @override
  void dispose() {
    super.dispose();
    inputController.dispose();
  }

  Widget panel(BuildContext context) => SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(40, 50, 40, 20),
                  child: Text(
                    "Create \naccount",
                    textAlign: TextAlign.left,
                    style: _getTextStyle(size: _titleSize),
                  ),
                ),
              ),
              buildTextInput(
                context: context,
                obscure: false,
                validator: inputController
                    .buildNamesValidator("The name can't be empty"),
                hint: "Input your name",
                controller: inputController.ctlNombres,
                label: "Name",
                icon: CupertinoIcons.person_alt,
              ),
              buildTextInput(
                context: context,
                obscure: false,
                validator: inputController
                    .buildNamesValidator("Last names can't be empty"),
                hint: "Input your lastname",
                controller: inputController.ctlApellidos,
                label: "Last names",
                icon: CupertinoIcons.person,
              ),
              buildTextInput(
                context: context,
                obscure: false,
                validator:
                    inputController.buildEmailValidator("Input a valid email"),
                hint: "Input your email address",
                controller: inputController.ctlEmail,
                label: "Your email",
                icon: CupertinoIcons.envelope_fill,
              ),
              buildTextInput(
                context: context,
                obscure: true,
                validator: (void value) {},
                hint: "Input password",
                controller: inputController.ctlPassword,
                label: "Password",
                icon: CupertinoIcons.lock_fill,
              ),
              buildTextInput(
                context: context,
                obscure: true,
                validator: inputController
                    .buildSecondPswdValidator("Passwords are not equal"),
                hint: "Input your password",
                controller: inputController.ctlSecondPassword,
                label: "Repeat password",
                icon: CupertinoIcons.lock,
              ),
              buildFormSubmit(context, _formKey),
              //TODO (Sign Up) Ingresar con Unidandes.
              //
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  children: [
                    Text("Already have an account?"),
                    TextButton(
                        onPressed: () {
                          context
                              .read<NavigationService>()
                              .navigateTo("SignIn");
                        },
                        child: Text("Log In")),
                  ],
                ),
              )
            ],
          ),
        ),
      );

  final Widget body =
      Container(decoration: BoxDecoration(gradient: CustomTheme.mainGradient));

  final double height = 800;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ScrollSheet(
            maxHeight: height,
            minHeight: height,
            isDraggable: false,
            panel: panel(context),
            body: body),
      ),
    );
  }

  Widget buildFormSubmit(BuildContext context, GlobalKey<FormState> _formKey) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: ElevatedButton(
        onPressed: () async {
          bool hasConnection =
              await ConnectionStatusService.getInstance().hasConnectionNow;
          if (!hasConnection) {
            print('No internet');
            showDialog(
                context: context,
                builder: (_) => authNoConnectionAlert(context));
          } else if (_formKey.currentState.validate()) {
            SignUpResult result = await context
                .read<AuthenticationService>()
                .signUp(
                    email: inputController.email,
                    password: inputController.password);

            if (result.status == AuthResult.SignedUp) {
              context.read<NavigationService>().flushNavigator();
              var dbManager = new DbManager();
              // Persists user info in Firebase
              dbManager.createUser(
                  result.credentials.user.uid, inputController.user);

              context
                  .read<NavigationService>()
                  .navigateTo("Goals", arguments: result.credentials.user.uid);
            } else {
              print("Error creating user");
              showDialog(
                  context: context, builder: (_) => badSignUpAlert(context));
              //TODO (Sign up) feedback error al crear cuenta
            }
          } else {
            print("Invalid");
          }
        },
        child: Text("REGISTER"),
      ),
    );
  }
}
