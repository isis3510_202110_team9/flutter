import 'dart:convert';

import 'package:bubble/bubble.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/config/watson_credentials.dart';
import 'package:gradua_app/model/chat_history_model.dart';
import 'package:gradua_app/model/message_model.dart';
import 'package:gradua_app/screens/player.dart';
import 'package:gradua_app/services/connection_status_service.dart';
import 'package:gradua_app/services/db_manager.dart';
import 'package:gradua_app/services/hive_manager.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

/// Chatbox to interact in the app
class Chatbot extends StatefulWidget {
  /// The name of the user wich will be interacting with the Chatbot
  @override
  _ChatbotState createState() => _ChatbotState();
}

class _ChatbotState extends State<Chatbot> {
  HiveManager hive = HiveManager.getInstance();

  /// The name of the user wich will be interacting with the Chatbot
  /// Dialogflow object
  bool holaSended;

  /// To knows if dialogflow object has been fetched
  bool dialogFlowFetched;

  ChatHistory historial = ChatHistory();

  var allMessages;

  /// Input for the users questions
  final messageInsert = TextEditingController();

  /// List of messages
  List<dynamic> messages = List();

  bool initialQuestion = false;
  bool isBoxOpened = false;
  bool messageState = false;
  Box boxChat;
  String uid;

  @override
  @protected
  @mustCallSuper
  void initState() {
    super.initState();
    historial.initDB().then((value) {
      dialogFlowFetched = false;
      holaSended = false;
      isBoxOpened = true;
      messageState = false;
      Map raw = historial.getAllMessages().toMap();
      print(raw.values.toList().runtimeType);
      messages = raw.values.toList();
      setState(() {
        messageState = true;
      });
    });
  }

  String session_id = "";

  var authn = 'Basic ' +
      //TODO Here you need to put the api key from Watson assistant in order to acces the chatbot

      base64Encode(
        utf8.encode('apikey:${watsonCredentials['apikey']}'),
      );
  setupChatbot() async {
    var urlPrueba = Uri.parse(
        "https://api.us-south.assistant.watson.cloud.ibm.com/instances/50b6ab4c-f5fb-40b9-8fec-a886d930f602/v2/assistants/bab10b84-37ba-48aa-9ec0-59f1d9d14786/sessions?version=2020-04-01");

    var newSess = await http.post(urlPrueba,
        headers: {'Content-Type': 'application/json', 'Authorization': authn});
    //print('Response status: ${newSess.statusCode}');
    //print('Response body: ${newSess.body}');

    try {
      if (newSess.statusCode != 201) {
        //throw Exception('Failed to load post');
        throw Exception('post error: statusCode= ${newSess.statusCode}');
      }
      setState(() {
        dialogFlowFetched = true;
      });
    } on Exception {
      print('Failed to load post');
      print(newSess.statusCode);
      setState(() {
        dialogFlowFetched = true;
      });
    }

    setState(() {
      dialogFlowFetched = true;
    });
    //Create a new session. A session is used to send user input to a skill and receive responses. It also maintains the state of the conversation.
    print(newSess.body);
    var parsedJsonSession = json.decode(newSess.body);
    print(parsedJsonSession);
    session_id = parsedJsonSession['session_id'];
    print('the session $session_id is created');
    if (dialogFlowFetched == true && holaSended == false) {
      setState(() {
        holaSended = true;
        messageState = false;
      });
    }
  }

  void updateMessages(Message msg) {
    messages.insert(0, msg);
    setState(() {
      messageState = true;
    });
  }

  sendMessage(String mensaje) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': authn,
    };
    var request = http.Request(
        'POST',
        Uri.parse(
            'https://api.us-south.assistant.watson.cloud.ibm.com/instances/50b6ab4c-f5fb-40b9-8fec-a886d930f602/v2/assistants/bab10b84-37ba-48aa-9ec0-59f1d9d14786/sessions/$session_id/message?version=2020-04-01'
            //'https://api.us-south.assistant.watson.cloud.ibm.com/instances/50b6ab4c-f5fb-40b9-8fec-a886d930f602/v2/assistants/d95e2ddd-2373-4866-93d8-5d252532f024/sessions/$session_id/message?version=2020-04-01'
            ));
    request.body = '''{"input": {"text": "''' + mensaje + '\"' '''}}''';
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    print(response.reasonPhrase);
    if (response.statusCode == 200) {
      var answerJson = await response.stream.bytesToString();

      var jsonToString = json.decode(answerJson);
      var toJson = jsonEncode(jsonToString);
      var decodeJson = jsonDecode(toJson);
      var output = decodeJson["output"];

      var genericJson = jsonEncode(output);
      var genericDecode = jsonDecode(genericJson);
      var finalAnswer = genericDecode["generic"][0]["text"];

      if (finalAnswer.toString().startsWith('{')) {
        Map messageMap = jsonDecode(finalAnswer);
        var meditation = Meditation.fromJson(messageMap);

        print("${meditation.fileLink}");
        print(meditation.meditationType);
        print('MEDITATION TYPE');
        var dataBase = DbManager();
        dataBase.incrementMeditationReproductions(meditation.meditationType);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return Player(
                  meditationId: meditation.meditationType,
                  audioPath: meditation.fileLink);
            },
          ),
        );
      } else {
        setState(() {
          Message watsonRespons =
              Message(0, finalAnswer.toString(), DateTime.now());
          if (isBoxOpened && watsonRespons != null) {
            historial.insert(watsonRespons);
            updateMessages(watsonRespons);
          } else {
            var now = DateTime.now();
            Message problem =
                Message(0, "Can you please refrace, I didn't understand", now);
            historial.insert(problem);
            updateMessages(problem);
          }
        });
      }
      print(toJson);
    } else {
      print(response.reasonPhrase);
    }
  }

  @override
  Widget build(BuildContext context) {
    uid = context.read<User>().uid;
    historial.initDB();
    Box box = historial.getAllMessages();
    if (dialogFlowFetched == false) {
      setupChatbot();
      if (box.length == 0) {
        Message initial = Message(0,
            "Hello. I am Gradúa. How are you feeling today?", DateTime.now());
        if (isBoxOpened) {
          historial.insert(initial);
          //messages.insert(0, initial);
          updateMessages(initial);
          // if (historial.getAllMessages().length > 1) {
          // }
        }
        setState(() {
          messageState = false;
        });
      }
    }
    ordenarFechas();
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Flexible(
          child: Align(
            alignment: Alignment.topCenter,
            child: ListView.builder(
              reverse: true,
              shrinkWrap: true,
              itemCount: messages.length,
              itemBuilder: (context, index) => renderChatBubble(
                  messages[index].message.toString(),
                  messages[index].data,
                  messages[index].time),
            ),
          ),
        ),
        Container(
          // padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
          // margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Row(
            children: <Widget>[
              Flexible(
                  child: Padding(
                padding: const EdgeInsets.fromLTRB(30, 0, 20, 0),
                child: TextField(
                  controller: messageInsert,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                      hintText: "Message...",
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 18.0)),
                ),
              )),
              Container(
                margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                decoration: BoxDecoration(
                    color: CustomTheme.graduaTeal,
                    borderRadius: BorderRadius.circular(10)),
                child: IconButton(
                    icon: Icon(
                      CupertinoIcons.location_fill,
                      size: 20.0,
                      color: Colors.white,
                    ),
                    onPressed: () async {
                      bool _connectionStatus =
                          await ConnectionStatusService.getInstance()
                              .hasConnection;
                      if (messageInsert.text.isEmpty) {
                        print("empty message");
                      }
                      setState(() {
                        Message userMes =
                            Message(1, messageInsert.text, DateTime.now());
                        historial.insert(userMes);
                        messages.insert(0, userMes);
                        hive.updateMessagesCount(uid, messages.length);
                      });
                      sendMessage(messageInsert.text);
                      if (_connectionStatus == false) {
                        setState(() {
                          Message userMes = Message(
                              0,
                              "You lost connection to Gradua. Try again later",
                              DateTime.now());
                          historial.insert(userMes);
                          messages.insert(0, userMes);
                        });
                      }
                      messageInsert.clear();
                    }),
              )
            ],
          ),
        ),
      ],
    );
  }

  void ordenarFechas() async {
    if (historial.getAllMessages() != null) {
      var values = historial.getAllMessages().values.toList();
      print('ANTES ORDENANDO');
      print(values.length);
      if (values.isNotEmpty && values.length > 1) {
        print('ORDENANDO');
        values.sort((a, b) => b.time.compareTo(a.time));
        messages = values;
      }
    }
  }
}

//for better one i have use the bubble package check out the pubspec.yaml

Widget renderChatBubble(String message, int data, DateTime time) {
  String finalTime = DateFormat("dd MMM  hh:mm a").format(time);

  return Padding(
    padding: EdgeInsets.all(10.0),
    child: Column(
      children: [
        Bubble(
          radius: Radius.circular(15.0),
          color: data == 0 ? Color(0xffe4e4e9) : CustomTheme.graduaTeal,
          elevation: 0.0,
          alignment: data == 0 ? Alignment.topLeft : Alignment.topRight,
          nip: data == 0 ? BubbleNip.leftBottom : BubbleNip.rightTop,
          child: Padding(
            padding: EdgeInsets.all(2.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  width: 10.0,
                ),
                Flexible(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: Text(
                          message,
                          style: TextStyle(
                              color: data == 0
                                  ? Colors.black
                                  : CustomTheme.purpleTextColor,
                              fontWeight: FontWeight.normal,
                              fontFamily: "Lato",
                              fontSize: 18),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Text(
          finalTime,
        )
      ],
    ),
  );
}

class Meditation {
  final String fileLink;
  final String meditationType;

  Meditation(this.fileLink, this.meditationType);
  Meditation.fromJson(Map<String, dynamic> json)
      : meditationType = json['responseType'],
        fileLink = json['file'];

  Map<String, dynamic> toJson() => {
        'file': fileLink,
        'responseType': meditationType,
      };
}

renderAnswer(String response) {
  String finalMessage;
  print(">> " + response.toString());
  return {
    "message": finalMessage,
  };
}
