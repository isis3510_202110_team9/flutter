import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:gradua_app/components/connection_alert.dart';
import 'package:gradua_app/layout/scroll_sheet.dart';
import 'package:gradua_app/screens/sign_up.dart';
import 'package:gradua_app/services/connection_status_service.dart';
import 'package:gradua_app/services/navigation_service.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:gradua_app/view_model/alerts_view_model.dart';
import 'package:provider/provider.dart';

//TODO (Welcome) los estilos aquí definidos deben camabiarse cuando la particula de diseño esté lista
class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  Widget panel(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        CircleAvatar(
          backgroundColor: Colors.white,
          backgroundImage: AssetImage("assets/images/isotipo.png"),
          radius: 47,
        ),
        Column(
          children: [
            Text(
              "Gradúa",
              style: TextStyle(
                  fontFamily: "Roboto",
                  fontSize: 48,
                  fontWeight: FontWeight.w700),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 35),
              child: Text(
                "Welcome to Gradua,\n your personal meditation app",
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: OutlinedButton(
                // style: PurpleElevatedButton,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SignUp()),
                  );
                },
                child: Text(
                  "Create an account",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                // style: PurpleOutlinedButton,
                onPressed: () {
                  context.read<NavigationService>().navigateTo("SignIn");
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => SignIn()),
                  // );
                },
                child: Text(
                  "Log In",
                ),
              ),
            ),
          ],
        ),
        TextButton(
          onPressed: () async {
            bool hasConnection =
                await ConnectionStatusService.getInstance().hasConnectionNow;
            if (hasConnection) {
              context
                  .read<NavigationService>()
                  .navigateTo("PlayerTTS", arguments: {"text": ""});
            } else {
              showDialog(
                  context: context,
                  builder: (_) => quickMeditationAlert(context));
            }
          },
          child: Text("START QUICK MEDITATION",
              style: TextStyle(
                  fontFamily: "Roboto",
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(149, 134, 168, 1))),
        )
      ],
    );
  }

  final Widget body =
      Container(decoration: BoxDecoration(gradient: CustomTheme.mainGradient));

  final double height = 550.0;

  @override
  Widget build(BuildContext context) {
    AlertsViewModel _alerts = AlertsViewModel(context);
    _alerts.activateConnectionAlert(
        ConnectionLostAlert(), ConnectionRestoredAlert());
    return SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            body: ScrollSheet(
                maxHeight: height,
                minHeight: height,
                isDraggable: false,
                panel: panel(context),
                body: body)));
  }
}
