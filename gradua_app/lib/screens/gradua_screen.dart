import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/layout/scroll_sheet.dart';
import 'package:gradua_app/screens/chat.dart';
import 'package:gradua_app/services/analytics_service.dart';
import 'package:gradua_app/text_style.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class Gradua extends StatefulWidget {
  @override
  _GraduaScreenState createState() => _GraduaScreenState();
}

class _GraduaScreenState extends State<Gradua> {
  Widget panel(contex) => Container();

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height * 0.7;
    final double widthDivider = (MediaQuery.of(context).size.width / 4);
    return Provider(
      create: (_) => PanelController(),
      child: ScrollSheet(
        controllerType: ControllerType.fromContext,
        // controller: controller,
        maxHeight: height,
        minHeight: 0,
        isDraggable: true,
        panel: Padding(
          padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
          child: Chat(),
        ),
        body: GraduaScreenBody(),
      ),
    );
  }
}

class GraduaScreenBody extends StatefulWidget {
  @override
  _GraduaScreenBodyState createState() => _GraduaScreenBodyState();
}

class _GraduaScreenBodyState extends State<GraduaScreenBody> {
  bool animateBlob = true;
  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.width);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    void handleFloatingButton() {
      context
          .read<AnalyticsService>()
          .logCustomEvent(name: "chatButtonPressed");
      PanelController controller = context.read<PanelController>();
      if (controller.isPanelClosed) {
        setState(() {
          animateBlob = false;
        });
        controller.animatePanelToPosition(1,
            curve: Cubic(0.17, 0.67, 0.83, 0.67),
            duration: Duration(milliseconds: 400));
      }
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                Lottie.asset(
                  'assets/animations/bgBlob.json',
                  width: width * 0.8,
                  repeat: false,
                  reverse: false,
                  animate: false,
                  fit: BoxFit.fill,
                ),
                Lottie.asset(
                  'assets/animations/wave.json',
                  repeat: true,
                  reverse: false,
                  animate: animateBlob,
                  width: MediaQuery.of(context).size.width * 0.7,
                  fit: BoxFit.fill,
                )
              ],
            ),
          ],
        ),
        Expanded(
          flex: 5,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FloatingActionButton(
                onPressed: handleFloatingButton,
                tooltip:
                    'This button changes the current screen to the chat screen',
                child: Icon(
                  CupertinoIcons.captions_bubble,
                  color: Color(0xFF545454),
                ),
                backgroundColor: Colors.white,
              ),
              SafeArea(
                child: Text(
                  'Can\' talk? No worries, we can chat!',
                  style: GraduaTextStyle,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
