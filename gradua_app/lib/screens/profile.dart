import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/components/profile_card.dart';
import 'package:gradua_app/model/chat_history_model.dart';
import 'package:gradua_app/model/profile_model.dart';
import 'package:gradua_app/services/authentication_service.dart';
import 'package:gradua_app/services/db_manager.dart';
import 'package:gradua_app/services/hive_manager.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

String _printDuration(int seconds) {
  Duration duration = new Duration(seconds: seconds);
  String twoDigits(int n) => n.toString().padLeft(2, "0");
  String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
  return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
}

class _ProfileState extends State<Profile> {
  HiveManager manager = HiveManager.getInstance();
  String names = "Loading...";
  String initials = '';
  List nameList = List();
  ProfileInformation currentUser;
  int messagesState;
  int sessionState;
  int timeState;
  int positiveState;
  int negativeState;
  int generatedState;

  Future<void> getNames() async {
    final uid = context.read<User>().uid;
    print('User uid: $uid');
    var db = new DbManager();
    var user = await db.getUser(uid);
    if (manager.getProfInfo(uid) == null) {
      manager.initializeProfileInfo(uid);
    }
    currentUser = manager.getProfInfo(uid);
    setState(() {
      this.names = toBeginningOfSentenceCase(user.data()["name"]) +
          " " +
          toBeginningOfSentenceCase(user.data()["lastname"]);
      nameList = names.split('\ ');
      initials = nameList[0].toString().substring(0, 1) +
          nameList[1].toString().substring(0, 1);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getNames().then((value) {
      messagesState = currentUser.messages;
      timeState = currentUser.time;
      positiveState = currentUser.positive;
      negativeState = currentUser.negative;
      generatedState = currentUser.createdM;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: CircleAvatar(
                backgroundColor: CustomTheme.graduaDarkPurple,
                child: Text(
                  initials,
                  style: TextStyle(
                    fontFamily: "Roboto",
                    fontSize: 40,
                    color: Colors.white,
                  ),
                ),
                radius: 45,
              ),
            ),
          ],
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            names,
            style: TextStyle(
              fontFamily: "Roboto",
              fontWeight: FontWeight.w500,
              fontSize: 25,
            ),
          ),
        ),
        if (currentUser != null)
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                    onPressed: () {
                      ChatHistory().deleteAll();
                      manager.deleteAll();
                      (context.read<AuthenticationService>().signOut());
                    },
                    child: Text("Sign out", style: TextStyle(fontSize: 20))),
                ProfileCard(
                  icon: Icon(
                    CupertinoIcons.wind_snow,
                    color: Colors.pinkAccent,
                    size: 30,
                  ),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 5),
                          child: Text(
                            "Sessions: ",
                            style: TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                        Text(currentUser.sessions.toString(),
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w600)),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Text("Tiempo ",
                              style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w300)),
                        ),
                        Text(_printDuration(timeState),
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  title: "Meditations",
                ),

                //Second card, includes the feelings

                ProfileCard(
                  icon: Icon(
                    CupertinoIcons.smiley,
                    color: Colors.blue,
                    size: 30,
                  ),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 5),
                              child: Text(
                                "Positive: ",
                                style: TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              ),
                            ),
                            Text(positiveState.toString(),
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                        Row(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 5),
                              child: Text(
                                "Negativo: ",
                                style: TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              ),
                            ),
                            Text(negativeState.toString(),
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ],
                    ),
                  ),
                  title: "Feelings",
                ),

                //Third cad, including the amount of messages you had with the chatbot
                ProfileCard(
                  icon: Icon(
                    CupertinoIcons.chat_bubble,
                    color: Colors.deepPurpleAccent,
                    size: 30,
                  ),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 5),
                          child: Text(
                            (messagesState.toString()),
                            style: TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  title: "Mensajes",
                ),
                ProfileCard(
                  icon: Icon(
                    CupertinoIcons.chat_bubble,
                    color: Colors.blue,
                    size: 30,
                  ),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 5),
                          child: Text(
                            (generatedState.toString()),
                            style: TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  title: "Created Meditations",
                ),
              ],
            ),
          ),
      ],
    );
  }
}
