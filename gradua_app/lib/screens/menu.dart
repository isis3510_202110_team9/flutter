import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/components/meditation_card.dart';
import 'package:gradua_app/services/connection_status_service.dart';
import 'package:gradua_app/services/db_manager.dart';
import 'package:gradua_app/services/hive_manager.dart';
import 'package:gradua_app/services/navigation_service.dart';
import 'package:gradua_app/styles/gradients.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

Widget createTopCard(isConnected, BuildContext context, bool connection) {
  return Card(
    color: Color(isConnected ? 0xffd5b3e6 : 0xffe4e2e8),
    margin: EdgeInsets.all(10),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20.0),
    ),
    child: Padding(
      padding: const EdgeInsets.only(left: 23.0, top: 16.0),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Meditations",
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Poppins'),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "According to your emotions, goals and interests",
              style: TextStyle(fontSize: 15, fontFamily: 'Poppins'),
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            child: Center(
              child: TextButton(
                child: Text("Create meditation",
                    style: TextStyle(fontSize: 14, fontFamily: 'Poppins')),
                style: ButtonStyle(
                  padding:
                      MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                  foregroundColor: MaterialStateProperty.all<Color>(
                      isConnected ? Colors.purple : Colors.grey),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(
                          color: isConnected ? Colors.purple : Colors.grey),
                    ),
                  ),
                ),
                onPressed: () {
                  if (!connection) return null;
                  context
                      .read<NavigationService>()
                      .navigateTo("PlayerTTS", arguments: {
                    "text": """Holaa""",
                  });
                },
              ),
              // onPressed: () => context.read<AnalyticsService>().logCustomEvent(name: "createMeditationButtonPressed")
              //  TODO. Necesito la variable context para el provider de analytics
            ),
          )
        ],
      ),
    ),
  );
}

Container buildSectionTitle(text) {
  return Container(
    margin: EdgeInsets.only(left: 10),
    child: Text(text,
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          fontFamily: 'Poppins',
        )),
  );
}

Function getCallbackBasedOnId(navigationService, String meditationId,
    hasInternetConnection, existsLocally, meditationPath, allMeditations) {
  return () {
    // TODO add case for favorites
    // context.read<AnalyticsService>().LogAddToFavorites();

    if (existsLocally) {
      navigationService.navigateTo("Player",
          arguments: {"label": meditationId, "link": meditationPath});

      if (hasInternetConnection) {
        // TODO let in player
        new DbManager()
            .incrementMeditationReproductions(meditationId.toLowerCase());
      }
    } else if (hasInternetConnection) {
      // TODO let in player
      new DbManager()
          .incrementMeditationReproductions(meditationId.toLowerCase());

      var meditationLink = "";
      // print('ALL MEDITATIONS');
      print(allMeditations.length);
      for (var i = 0; i < allMeditations.length && meditationLink == ''; i++) {
        // print('LLEGÓ AL FOR');
        print(allMeditations[i]);
        if (meditationId != null &&
            allMeditations[i]['label'] != null &&
            meditationId.toLowerCase() ==
                allMeditations[i]['label'].toLowerCase()) {
          print('LLEGÓ AL IF: ${allMeditations[i].toString()}');
          meditationLink = allMeditations[i]['link'];
        }
      }

      navigationService.navigateTo("Player", arguments: {
        "label": meditationId.toLowerCase(),
        "link": meditationLink
      });
    }
  };
}

Function getCallbackBasedOnObject(navigationService, meditation,
    hasInternetConnection, existsLocally, localPath) {
  return () {
    // TODO add case for favorites
    // context.read<AnalyticsService>().LogAddToFavorites();

    if (existsLocally) {
      navigationService.navigateTo("Player",
          arguments: {"label": meditation['label'], "link": localPath});

      if (hasInternetConnection) {
        // TODO let in player
        new DbManager().incrementMeditationReproductions(meditation['id']);
      }
    } else if (hasInternetConnection) {
      // TODO let in player
      new DbManager().incrementMeditationReproductions(meditation['id']);

      navigationService.navigateTo("Player", arguments: {
        "label": meditation['label'].toLowerCase(),
        "link": meditation['link']
      });
    }
  };
}

class _MenuState extends State<Menu> {
  var _allMeditations = [];
  var _popularMeditations = [];
  var _favoriteMeditations = [];
  bool initConnection;
  HiveManager _preferences = HiveManager.getInstance();

  String getMeditationLocalLink(meditationId) {
    var path =
        "https://firebasestorage.googleapis.com/v0/b/gradua-49351.appspot.com/o/breathing.mp3?alt=media&token=c8debd04-497e-4194-a453-d6d3f410160c";
    try {
      print("va a buscar la ruta de $meditationId");
      path = _preferences.getLocalPathIfExist(meditationId.toLowerCase());
      print(path);
    } catch (e) {} finally {
      return path;
    }
  }

  // REMAKE TARAKE TAKE
  Widget buildCardsBasedOnId(
      navigationService, meditationsId, hasInternetConnection, direction) {
    List<Widget> cardMeditations = [
      MeditationCard(
          title: "loading", disabled: true, isLarge: direction == 'vertical')
    ];

    if (meditationsId != null && meditationsId.length != 0) {
      cardMeditations.clear();
      meditationsId.forEach((meditationId) {
        var meditationPath = getMeditationLocalLink(meditationId);
        bool existsLocally = false;

        if (meditationPath != null) {
          existsLocally = File(meditationPath).existsSync();
        }

        cardMeditations.add(MeditationCard(
            title: meditationId,
            disabled: hasInternetConnection || existsLocally,
            onTap: getCallbackBasedOnId(
                navigationService,
                meditationId,
                hasInternetConnection,
                existsLocally,
                meditationPath,
                _allMeditations),
            isLarge: direction == 'vertical'));
      });
    }

    if (direction == 'horizontal') {
      return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(children: cardMeditations),
      );
    } else {
      return Column(children: cardMeditations);
    }
  }

  Widget buildCardsBasedOnObject(
      navigationService, meditations, hasInternetConnection, direction) {
    print(meditations);
    List<Widget> cardMeditations = [
      MeditationCard(
          title: "loading", disabled: true, isLarge: direction == 'vertical')
    ];

    if (meditations != null && meditations.length != 0) {
      cardMeditations.clear();
      meditations.forEach((meditationObj) {
        var localPath =
            getMeditationLocalLink(meditationObj['label'].toLowerCase());
        bool existsLocally = false;

        if (localPath != null) {
          existsLocally = File(localPath).existsSync();
        }

        cardMeditations.add(
          MeditationCard(
              title: meditationObj['label'],
              disabled: hasInternetConnection || existsLocally,
              isLarge: direction == 'vertical',
              onTap: getCallbackBasedOnObject(navigationService, meditationObj,
                  hasInternetConnection, existsLocally, localPath)),
        );
      });
    }

    if (direction == 'horizontal') {
      return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(children: cardMeditations),
      );
    } else {
      return Column(children: cardMeditations);
    }
  }

  @override
  initState() {
    if (_popularMeditations.length == 0) {
      new DbManager()
          .getPopularMeditations()
          .then((response) => setState(() => {_popularMeditations = response}));
    }
    if (_allMeditations.length == 0) {
      new DbManager().getAllMeditations().then((response) {
        setState(() => {_allMeditations = response});
      });
    }
    setState(() {
      this.initConnection = ConnectionStatusService.getInstance().hasConnection;
    });
    super.initState();
  }

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh(firebaseuser) async {
    if (mounted) {
      var dbManager = new DbManager();

      dbManager
          .getPopularMeditations()
          .then((response) => setState(() => {_popularMeditations = response}));

      dbManager.getAllMeditations().then((meditations) {
        setState(() => {_allMeditations = meditations});
        dbManager.getFavoritesMeditationsId(firebaseuser).then((response) {
          setState(() {
            _favoriteMeditations = response;
          });
          HiveManager.getInstance().insertAllFavorites(_favoriteMeditations);
          print(response);
        });
      });
    }
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    final firebaseuser = context.watch<User>().uid;
    final navigationService = context.read<NavigationService>();

    if (_favoriteMeditations.length == 0 && _allMeditations.length == 0) {
      new DbManager().getFavoritesMeditationsId(firebaseuser).then((response) {
        setState(() {
          _favoriteMeditations = response;
        });
        HiveManager.getInstance().insertAllFavorites(_favoriteMeditations);
      });
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        decoration: BoxDecoration(gradient: SecondaryGradient),
        child: StreamBuilder<bool>(
          initialData: initConnection,
          stream: ConnectionStatusService.getInstance().connectionChange,
          builder: (context, connection) {
            bool isConnected = connection.data != null ? connection.data : true;

            return SmartRefresher(
                enablePullDown: true,
                enablePullUp: false,
                header: WaterDropHeader(),
                footer: CustomFooter(
                    builder: (BuildContext context, LoadStatus mode) {
                  Widget body;
                  if (mode == LoadStatus.idle) {
                    body = Text("pull up load");
                  } else if (mode == LoadStatus.loading) {
                    body = CupertinoActivityIndicator();
                  } else if (mode == LoadStatus.failed) {
                    body = Text("Load Failed!Click retry!");
                  } else if (mode == LoadStatus.canLoading) {
                    body = Text("release to load more");
                  } else {
                    body = Text("No more Data");
                  }
                  return Container(
                    height: 55.0,
                    child: Center(child: body),
                  );
                }),
                controller: _refreshController,
                onRefresh: () => _onRefresh(firebaseuser),
                onLoading: _onLoading,
                child: ListView(
                  children: [
                    createTopCard(isConnected, context, connection.data),
                    if (_favoriteMeditations.length != 0)
                      buildSectionTitle("Your favorites"),
                    if (_favoriteMeditations.length != 0)
                      buildCardsBasedOnId(navigationService,
                          _favoriteMeditations, isConnected, "horizontal"),
                    buildSectionTitle("Popular meditations"),
                    buildCardsBasedOnObject(navigationService,
                        _popularMeditations, isConnected, "horizontal"),
                    buildSectionTitle("All meditations"),
                    buildCardsBasedOnObject(navigationService, _allMeditations,
                        isConnected, "vertical"),
                  ],
                ));
          },
        ),
      ),
    );
  }
}
