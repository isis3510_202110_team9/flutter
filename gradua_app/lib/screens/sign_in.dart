import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/components/connection_alert.dart';
import 'package:gradua_app/components/text_input.dart';
import 'package:gradua_app/layout/scroll_sheet.dart';
import 'package:gradua_app/services/analytics_service.dart';
import 'package:gradua_app/services/authentication_service.dart';
import 'package:gradua_app/services/connection_status_service.dart';
import 'package:gradua_app/services/db_manager.dart';
import 'package:gradua_app/services/hive_manager.dart';
import 'package:gradua_app/services/navigation_service.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:gradua_app/styles/gradients.dart';
import 'package:gradua_app/view_model/user_view_model.dart';
import 'package:provider/provider.dart';

//TODO (SignIn) los estilos aquí definidos deben camabiarse cuando la particula de diseño esté lista
class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();

  final double _titleSize = 45.0;

  final double _subtitleSize = 18.0;

  // final TextEditingController _emailController = TextEditingController();
  // final TextEditingController _passwordController = TextEditingController();

  UserAuthViewModel inputController = UserAuthViewModel();

  @override
  void dispose() {
    super.dispose();
    inputController.dispose();
  }

  TextStyle _getTextStyle({double size}) {
    return TextStyle(
        fontFamily: "Roboto", fontWeight: FontWeight.bold, fontSize: size);
  }

  String Function(String) _buildEmailValidator(String errorMsg) =>
      (value) => EmailValidator.validate(value) ? null : ("Type a valid email");

  final Widget body =
      Container(decoration: BoxDecoration(gradient: MainGradient));

  @override
  Widget build(BuildContext context) {
    Widget panel() => SingleChildScrollView(
          child: (Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 50, horizontal: 40),
                    child: Text(
                      "Sign In",
                      textAlign: TextAlign.left,
                      style: _getTextStyle(size: _titleSize),
                    ),
                  ),
                ),
                buildTextInput(
                  context: context,
                  icon: CupertinoIcons.envelope_fill,
                  obscure: false,
                  validator: _buildEmailValidator("This email isn't valid"),
                  controller: inputController.ctlEmail,
                  label: "Your email",
                  hint: "Input your email address",
                ),
                buildTextInput(
                    context: context,
                    icon: CupertinoIcons.lock_fill,
                    obscure: true,
                    validator: (void value) {},
                    hint: "Type your password",
                    label: "Password",
                    controller: inputController.ctlPassword),
                buildFormSubmit(context),
                // //TODO Ingresar con Unidandes.
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: Row(
                    children: [
                      Text("¿Don't have account?"),
                      TextButton(
                          onPressed: () {
                            context.read<AnalyticsService>().logCustomEvent(
                                name: "SignInSignUp",
                                parameters: {
                                  "info": "It went to signup from signin"
                                });
                            context
                                .read<NavigationService>()
                                .navigateTo("SignUp");
                          },
                          child: Text("Create your account")),
                    ],
                  ),
                )
              ],
            ),
          )),
        );

    final Widget body = Container(
        decoration: BoxDecoration(gradient: CustomTheme.mainGradient));

    final double height = 570;

    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ScrollSheet(
            maxHeight: height,
            minHeight: height,
            isDraggable: false,
            panel: panel(),
            body: body),
      ),
    ));
  }

  Widget buildFormSubmit(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
      child: ElevatedButton(
        onPressed: () async {
          DbManager db = DbManager();
          HiveManager hive = HiveManager.getInstance();
          await context.read<AnalyticsService>().logLogIn();
          bool hasConnection =
              await ConnectionStatusService.getInstance().hasConnectionNow;
          print('connection data: $hasConnection');
          if (!hasConnection) {
            print('No internet');
            showDialog(
                context: context,
                builder: (_) => authNoConnectionAlert(context));
          } else if (_formKey.currentState.validate()) {
            print(
                'Email: ${inputController.email} pass: ${inputController.password}');
            AuthResult result = await context
                .read<AuthenticationService>()
                .signIn(
                    email: inputController.email,
                    password: inputController.password);

            if (result == AuthResult.SignedIn) {
              context.read<NavigationService>().flushNavigator();
              String uid = context.read<User>().uid;
              context.read<AnalyticsService>().setUserProperties(uid: uid);
              var user = await db.getUser(uid);
              String name = user.data()["name"];
              String lastName = user.data()["lastname"];
              hive.insertNames(name, lastName: lastName);
              hive.initializeProfileInfo(uid);
            } else {
              //TODO (SignIn) feedback para cuando el login es incorrecto
              print("Error signing");
              showDialog(
                  context: context, builder: (_) => badSignInAlert(context));
            }
          } else {
            print("Not valid");
          }
        },
        child: Text("SIGN IN"),
      ),
    );
  }
}
