import 'package:disk_space/disk_space.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/components/ambient_noise.dart';
import 'package:gradua_app/components/buttons.dart';
import 'package:gradua_app/components/connection_alert.dart';
import 'package:gradua_app/components/formatted_disk_space.dart';
import 'package:gradua_app/model/player_model.dart';
import 'package:gradua_app/services/analytics_service.dart';
import 'package:gradua_app/services/connection_status_service.dart';
import 'package:gradua_app/services/hive_manager.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:gradua_app/view_model/player_view_model.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

class Player extends StatefulWidget {
  HiveManager manager = HiveManager.getInstance();
  final String meditationId;
  final String audioPath;

  Player({Key key, @required this.audioPath, @required this.meditationId})
      : super(key: key);

  @override
  _PlayerState createState() =>
      _PlayerState(audioPath: this.audioPath, meditationId: this.meditationId);
}

class _PlayerState extends State<Player> with SingleTickerProviderStateMixin {
  String audioPath;
  final String meditationId;
  HiveManager hive;
  bool amIfavorite = false;
  double _diskSpace = 0;
  bool downloading = false;
  bool removing = false;
  String uid;
  _PlayerState({this.audioPath, this.meditationId});

  @override
  void initState() {
    super.initState();
    initDiskSpace();
    hive = HiveManager.getInstance();

    bool isIn = hive.amIFavorite(this.meditationId);
    print('Init state');
    setState(() {
      print('IS_IN: $isIn');
      this.amIfavorite = isIn == null ? false : isIn;
    });

    uid = context.read<User>().uid;

    if (this.meditationId.toLowerCase() == "anxiety") {
      hive.updateNegativeEmotions(uid);
    } else if (this.meditationId.toLowerCase() == "balance") {
      hive.updateNegativeEmotions(uid);
    } else if (this.meditationId.toLowerCase() == "breathing") {
      hive.updateNegativeEmotions(uid);
    } else if (this.meditationId.toLowerCase() == "happiness") {
      hive.updatePositiveEmotions(uid);
    } else if (this.meditationId.toLowerCase() == "concentration") {
      hive.updatePositiveEmotions(uid);
    } else if (this.meditationId.toLowerCase() == "attention") {
      hive.updatePositiveEmotions(uid);
    }
  }

  Future<void> initDiskSpace() async {
    double diskSpace = 0;
    print('DiskSpace 1');
    diskSpace = await DiskSpace.getFreeDiskSpace;
    print('DiskSpace');
    print(diskSpace);

    // if (!mounted) return;

    setState(() {
      print('cambiando estado: ${diskSpace}');
      _diskSpace = diskSpace;
    });
  }

  @override
  Widget build(BuildContext context) {
    uid = context.read<User>().uid;

    PlayerViewModel vm = PlayerViewModel(audioPath);
    double width = MediaQuery.of(context).size.width;
    bool amILocal = hive.checkMeditationFileExists(this.meditationId);

    Future handleDownload() async {
      setState(() => downloading = true);
      Map result =
          await vm.downloadMeditation(this.meditationId, this.audioPath);

      if (result['success']) {
        context.read<AnalyticsService>().downloadMeditation();
        setState(() {
          downloading = false;
          amILocal = true;
          this.audioPath = result['path'];
        });
      } else {
        setState(() {
          downloading = false;
        });
      }
    }

    Future confirmedRemoval() async {
      context.read<AnalyticsService>().deleteDownloadedMeditation();
      setState(() => removing = true);
      bool result = await vm.deleteDownloadedMeditation(this.meditationId);
      if (result) {
        setState(() {
          amILocal = false;
          removing = false;
        });
        //  TODO cómo actualizo el path?
      } else {
        //  TODO handle error
      }
    }

    Future<Function> handleRemoveDownloaded() async {
      bool hasConnection =
          await ConnectionStatusService.getInstance().hasConnectionNow;
      if (hasConnection) {
        return confirmedRemoval;
      } else {
        return () {
          showDialog(
              context: context,
              builder: (_) => playerDeletAlert(context, confirmedRemoval));
        };
      }
    }

    Future handlePlay() async {
      bool hasConnection =
          await ConnectionStatusService.getInstance().hasConnectionNow;
      if (!amILocal && !hasConnection) {
        showDialog(context: context, builder: (_) => playerPlayAlert(context));
      } else {
        context.read<AnalyticsService>().logCustomEvent(
              name: "playButtonPressed",
            );
        vm.play();
      }
    }

    void handleAddToFavorites() {
      context.read<AnalyticsService>().LogAddToFavorites();
      vm.addToFavorites(context.read<User>().uid,
          this.meditationId.toLowerCase(), this.audioPath);
      setState(() {
        this.amIfavorite = true;
      });
    }

    void handleRemoveFavorite() {
      context.read<AnalyticsService>().LogRemoveFromFavorites();
      vm.removeFromFavorites(
          context.read<User>().uid, this.meditationId.toLowerCase());
      setState(() {
        this.amIfavorite = false;
      });
    }

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(gradient: CustomTheme.playerBlueGradient),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: StreamBuilder<PlayerModel>(
            stream: vm.playerStream,
            initialData: PlayerModel(audioPath: audioPath),
            builder: (context, player) {
              return Stack(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            width * 0.1, 90, width * 0.1, 0),
                        child: Lottie.asset(
                          'assets/animations/bgBlob.json',
                          repeat: true,
                          reverse: false,
                          animate: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            width * 0.15, 100, width * 0.15, 0),
                        child: Lottie.asset(
                          'assets/animations/wave.json',
                          repeat: true,
                          reverse: false,
                          animate: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    child: IconButton(
                      onPressed: () {
                        vm.onComplete();
                        Navigator.pop(context);
                        if (player.data.duration != null) {
                          hive.updateTime(uid, player.data.duration.inSeconds);
                        }
                      },
                      icon: Icon(CupertinoIcons.xmark),
                    ),
                    top: 20,
                    left: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Visibility(
                                    visible: this.meditationId != null,
                                    child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 40, 0, 0),
                                        child: Text(
                                          "Your are listening to: ${this.meditationId}",
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            color: Colors.green,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        )
                                        // AmbientNoiseWidget(),
                                        ),
                                  ),
                                ],
                              ),
                              Visibility(
                                visible: amILocal,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 0, 10, 0),
                                      child: Icon(
                                        CupertinoIcons.arrow_down_circle,
                                        color: Colors.green,
                                        size: 20,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                      child: formattedFileSize(vm.fileSize),
                                    ),
                                    formattedDiskSpace(_diskSpace)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Stack(
                                  children: [
                                    Positioned(
                                      left: width * 0.05,
                                      child: Visibility(
                                        visible: this.amIfavorite,
                                        child: FilledHeartButton(
                                          handleRemoveFavorite,
                                        ),
                                        replacement: OutlinedHeartButton(
                                          handleAddToFavorites,
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: player.data.isPaused ||
                                              player.data.isStopped
                                          ? playButton(player.data.isPlaying
                                              ? null
                                              : handlePlay)
                                          : SizedBox.shrink(),
                                    ),
                                    Center(
                                      child: Visibility(
                                        visible: player.data.isPlaying,
                                        child: pauseButton(player.data.isPlaying
                                            ? () {
                                                context
                                                    .read<AnalyticsService>()
                                                    .logCustomEvent(
                                                      name:
                                                          "pauseButtonPressed",
                                                    );
                                                vm.pause();
                                              }
                                            : null),
                                      ),
                                    ),
                                    Positioned.fill(
                                      right: width * 0.1,
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Visibility(
                                          visible: amILocal,
                                          child: Visibility(
                                            visible: !removing,
                                            child: deleteDownloadedButton(
                                                handleRemoveDownloaded()),
                                            replacement:
                                                CircularProgressIndicator(
                                              backgroundColor: Colors.redAccent,
                                            ),
                                          ),
                                          replacement: Visibility(
                                            visible: !downloading,
                                            // Si no está descargando mostrar botón para descargar
                                            child:
                                                downloadButton(handleDownload),
                                            replacement:
                                                CircularProgressIndicator(
                                              backgroundColor: Colors.blue,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                              child: Stack(
                                children: [
                                  Slider(
                                    activeColor: Colors.white,
                                    inactiveColor:
                                        Color.fromRGBO(137, 144, 154, 1),
                                    onChanged: (v) {
                                      final Position = v *
                                          player.data.duration.inMilliseconds;
                                      player.data.audioPlayer.seek(
                                        Duration(
                                          milliseconds: Position.round(),
                                        ),
                                      );
                                    },
                                    value: player.data.sliderValue,
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 70),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(30, 0, 30, 0),
                                    child: Text(
                                      player.data.sliderPositionText,
                                      style: CustomTheme.playerSliderTextStyle,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 30),
                                    child: Text(
                                      player.data.sliderDurationText,
                                      style: CustomTheme.playerSliderTextStyle,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    child: AmbientNoiseWidget(),
                    top: 20,
                    right: 10,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
