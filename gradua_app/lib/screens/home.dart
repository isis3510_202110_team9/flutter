import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/components/connection_alert.dart';
import 'package:gradua_app/screens/gradua_screen.dart';
import 'package:gradua_app/screens/menu.dart';
import 'package:gradua_app/screens/profile.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:gradua_app/view_model/alerts_view_model.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;

  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final firebaseuser = context.watch<User>();
    var user = firebaseuser.uid;
    AlertsViewModel _alerts = AlertsViewModel(context);
    _alerts.activateConnectionAlert(
        ConnectionLostAlert(), ConnectionRestoredAlert());
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: <Widget>[
          Menu(),
          Gradua(),
          Profile(),
        ].elementAt(_selectedIndex),
        bottomNavigationBar: Container(
          height: 80,
          decoration: BoxDecoration(boxShadow: [CustomTheme.mainShadow]),
          child: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  label: "Menu",
                  icon: Icon(
                    CupertinoIcons.circle,
                    size: 27,
                  )),
              BottomNavigationBarItem(
                  label: "Gradua",
                  icon: Icon(
                    CupertinoIcons.mic,
                    size: 27,
                  )),
              BottomNavigationBarItem(
                icon: Icon(
                  CupertinoIcons.person_2,
                  size: 27,
                ),
                label: "Profile",
              ),
            ],
            currentIndex: _selectedIndex,
            onTap: _onItemTapped,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            type: BottomNavigationBarType.fixed,
            iconSize: 40,
            selectedItemColor: CustomTheme.graduaTeal,
          ),
        ),
      ),
    );
  }
}
