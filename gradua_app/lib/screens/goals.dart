import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gradua_app/components/connection_alert.dart';
import 'package:gradua_app/services/db_manager.dart';
import 'package:gradua_app/services/navigation_service.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:gradua_app/styles/gradients.dart';
import 'package:gradua_app/view_model/goals_view_model.dart';
import 'package:provider/provider.dart';

class SignInGoals extends StatefulWidget {
  final String uuid;
  SignInGoals({Key key, @required this.uuid}) : super(key: key);

  @override
  _SignInGoalsState createState() => _SignInGoalsState(uuid: this.uuid);
}

class _SignInGoalsState extends State<SignInGoals> {
  final _formKey = GlobalKey<FormState>();
  final String uuid;
  _SignInGoalsState({@required this.uuid});

  GoalsViewModel goalsVM = GoalsViewModel();

  bool aceptaTerminos = false;

  TextStyle _getTextStyle({double size}) {
    return TextStyle(
        fontFamily: "Roboto",
        fontWeight: FontWeight.bold,
        fontSize: size,
        height: 1.2);
  }

  List<String> getSelectedGoals(List<bool> selected, List<String> goals) {
    List<String> selectedGoals = [];
    for (var i = 0; i < selected.length; i++) {
      if (selected[i]) {
        selectedGoals.add(goals[i]);
      }
    }
    return selectedGoals;
  }

  Widget _buildOption(BuildContext, int index) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Stack(
          children: [
            TextButton(
              onPressed: () {
                setState(() {
                  goalsVM.changeSelection(index);
                });
              },
              style: TextButton.styleFrom(
                backgroundColor: goalsVM.getSelection(index)
                    ? CustomTheme.graduaPurple
                    : HSVColor.fromColor(CustomTheme.graduaPurple)
                        .withAlpha(0.8)
                        .toColor(),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: constraints.maxWidth * 0.1,
                    vertical: constraints.maxHeight * 0.05),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AutoSizeText(
                      goalsVM.getName(index),
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          height: 1.2),
                      minFontSize: 10,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Icon(
                        goalsVM.getIcon(index),
                        size: constraints.maxHeight * 0.3,
                        color: Colors.white,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Visibility(
              visible: goalsVM.getSelection(index),
              child: TextButton(
                onPressed: () {
                  setState(() {
                    goalsVM.changeSelection(index);
                  });
                },
                style: TextButton.styleFrom(
                  backgroundColor: Color.fromARGB(70, 0, 0, 0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25),
                  ),
                ),
                child: Center(
                  child: Icon(
                    CupertinoIcons.check_mark_circled_solid,
                    size: constraints.maxWidth * 0.15,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  double getHeight(BuildContext context) {
    double size = MediaQuery.of(context).size.height;
    var padding = MediaQuery.of(context).padding;
    return size - padding.top - padding.bottom;
  }

  double getOptionAspectRatio(double height, double width) {
    return (width / 2) / ((height - 320) / 4);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final double _titleSize = 28.0;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          decoration: BoxDecoration(gradient: MainGradient),
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: getHeight(context) - 20,
                  margin: EdgeInsets.all(0),
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(246, 245, 245, 1),
                      borderRadius: BorderRadius.vertical(
                          top: Radius.circular(40), bottom: Radius.zero)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Tell us about what \nmotivates you...",
                          textAlign: TextAlign.left,
                          style: _getTextStyle(
                            size: _titleSize,
                          ),
                        ),
                      ),
                      Flexible(
                        child: GridView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.fromLTRB(0, 2, 0, 10),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: getOptionAspectRatio(
                                getHeight(context), width - 40),
                            mainAxisSpacing: 12.0,
                            crossAxisSpacing: 12.0,
                          ),
                          itemCount: goalsVM.goals.length,
                          itemBuilder: _buildOption,
                        ),
                      ),
                      CheckboxListTile(
                          checkColor: Colors.white,
                          selectedTileColor: CustomTheme.graduaTeal,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          subtitle: Text(
                              "I have read and accept the Terms of Service and Privacy Policies"),
                          controlAffinity: ListTileControlAffinity.leading,
                          value: aceptaTerminos,
                          onChanged: (value) =>
                              setState(() => aceptaTerminos = value)),
                      //TODO agregar términos y condiciones cuando los haya
                      buildFormSubmit(
                          context,
                          _formKey,
                          getSelectedGoals(
                              goalsVM.selected, goalsVM.goalsNames),
                          aceptaTerminos,
                          uuid),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Padding buildFormSubmit(BuildContext context, GlobalKey<FormState> _formKey,
    List<String> seleccionados, bool aceptaTerminos, String uuid) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    child: ElevatedButton(
      onPressed: !aceptaTerminos
          ? () {
              showDialog(
                  context: context,
                  builder: (_) => termsAndServicesAlert(context));
            }
          : () {
              print("Goals seleccionados: " + seleccionados.toString());
              var db = new DbManager();
              db.persistInterests(uuid, seleccionados);
              context.read<NavigationService>().flushNavigator();
            },
      child: Text("NEXT"),
    ),
  );
}
