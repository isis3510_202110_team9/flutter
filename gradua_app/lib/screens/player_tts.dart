import 'dart:convert';

import 'package:disk_space/disk_space.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/components/ambient_noise.dart';
import 'package:gradua_app/components/buttons.dart';
import 'package:gradua_app/components/formatted_disk_space.dart';
import 'package:gradua_app/model/player_tts_model.dart';
import 'package:gradua_app/services/analytics_service.dart';
import 'package:gradua_app/services/hive_manager.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:gradua_app/view_model/player_tts_view_model.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

class PlayerTTS extends StatefulWidget {
  // final String meditationId;
  final String text;

  PlayerTTS({Key key, @required this.text}) : super(key: key);

  @override
  _PlayerTTSState createState() => _PlayerTTSState(text: this.text);
}

class _PlayerTTSState extends State<PlayerTTS> {
  String text;
  double _diskSpace = 0;
  PlayerTTSViewModel vm;
  // final String meditationId;
  bool amIfavorite = false;
  HiveManager hive = HiveManager.getInstance();
  String uid;

  _PlayerTTSState({this.text});

  @override
  void initState() {
    super.initState();
    initDiskSpace();
    print('Init tts player state');
    String userName = hive.getNames()['name'];
    userName = userName == null ? 'Nuevo Usuario' : userName;
    print('User name: ' + userName);
    print('Iniciando request');
    var url = Uri.parse(
        'https://gradua.herokuapp.com/meditations/random?name=' + userName);
    http.get(url).then((value) {
      print('Request resolved');
      try {
        Map body = json.decode(value.body);
        print(body);
        text = body['meditation'];
        this.vm.changeText(text);
      } catch (e) {}
    });

    if (context.read<User>() != null && context.read<User>().uid != null) {
      uid = context.read<User>().uid;
      hive.updateCreatedCounter(uid);
    }
  }

  Future<void> initDiskSpace() async {
    double diskSpace = 0;
    print('DiskSpace init');
    diskSpace = await DiskSpace.getFreeDiskSpace;
    print('DiskSpace');
    print(diskSpace);

    setState(() {
      _diskSpace = diskSpace;
    });
  }

  @override
  Widget build(BuildContext context) {
    double diskSpace = _diskSpace;
    vm = PlayerTTSViewModel(text);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).viewPadding.top -
        MediaQuery.of(context).viewPadding.bottom;

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(gradient: CustomTheme.playerBlueGradient),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: StreamBuilder<PlayerTTSModel>(
            stream: vm.playerStream,
            initialData: PlayerTTSModel(text: text),
            builder: (context, player) {
              return Stack(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            width * 0.1, 90, width * 0.1, 0),
                        child: Lottie.asset(
                          'assets/animations/bgBlob.json',
                          repeat: true,
                          reverse: false,
                          animate: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            width * 0.15, 100, width * 0.15, 0),
                        child: Lottie.asset(
                          'assets/animations/wave.json',
                          repeat: true,
                          reverse: false,
                          animate: true,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    child: IconButton(
                      onPressed: () {
                        vm.onComplete();
                        Navigator.pop(context);
                      },
                      icon: Icon(CupertinoIcons.xmark),
                    ),
                    top: 20,
                    left: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 40, 0, 0),
                                      child: Text(
                                        "Generated Meditation",
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          color: Colors.green,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      )
                                      // AmbientNoiseWidget(),
                                      ),
                                ],
                              ),
                              Center(
                                child: formattedDiskSpace(_diskSpace),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Stack(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          0, 0, 0, height * 0.2),
                                      child: Center(
                                        child: player.data.isPaused ||
                                                player.data.isStopped
                                            ? playButton(player.data.isPlaying
                                                ? null
                                                : () {
                                                    context
                                                        .read<
                                                            AnalyticsService>()
                                                        .logCustomEvent(
                                                          name:
                                                              "playButtonPressed",
                                                        );
                                                    vm.play();
                                                  })
                                            : SizedBox.shrink(),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          0, 0, 0, height * 0.2),
                                      child: Center(
                                        child: player.data.isPlaying
                                            ? pauseButton(player.data.isPlaying
                                                ? () {
                                                    context
                                                        .read<
                                                            AnalyticsService>()
                                                        .logCustomEvent(
                                                          name:
                                                              "pauseButtonPressed",
                                                        );
                                                    vm.stop();
                                                  }
                                                : null)
                                            : SizedBox.shrink(),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    child: AmbientNoiseWidget(),
                    top: 20,
                    right: 10,
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
