import 'package:hive/hive.dart';
part 'profile_model.g.dart';

@HiveType(typeId: 2)
class ProfileInformation extends HiveObject {
  @HiveField(0)
  String uId;
  @HiveField(1)
  int sessions;
  @HiveField(2)
  int time;
  @HiveField(3)
  int positive;
  @HiveField(4)
  int negative;
  @HiveField(5)
  int messages;
  @HiveField(6)
  int createdM;

  ProfileInformation(this.uId, this.sessions, this.time, this.positive,
      this.negative, this.messages, this.createdM);
}
