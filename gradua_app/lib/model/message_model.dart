import 'package:hive/hive.dart';
part 'message_model.g.dart';

@HiveType(typeId: 0)
class Message extends HiveObject {
  @HiveField(0)
  int data;
  @HiveField(1)
  String message;
  @HiveField(2)
  var time;

  Message(this.data, this.message, this.time);
}
