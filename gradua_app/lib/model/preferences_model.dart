import 'package:gradua_app/model/meditation_route_model.dart';
import 'package:hive/hive.dart';
part 'preferences_model.g.dart';

@HiveType(typeId: 1)
class Preferences extends HiveObject {
  //@HiveField(0)
  //List<String> interests;
  @HiveField(0)
  List<MeditationRoute> ruta;

  Preferences(this.ruta);
}
