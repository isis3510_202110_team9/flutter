import 'package:flutter/cupertino.dart';

class GoalModel {
  String name;
  IconData icon;
  bool isSelected;

  GoalModel(this.name, this.icon, {this.isSelected = false});

  void changeSelection() {
    this.isSelected = !this.isSelected;
  }

  void select() {
    this.isSelected = true;
  }

  void unselect() {
    this.isSelected = false;
  }
}
