import 'package:gradua_app/model/message_model.dart';
import 'package:hive/hive.dart';

class ChatHistory {
  Box chatHistory;
  Box segundo;
  Future initDB() async {
    await Hive.openBox('chatHistoryL');

    chatHistory = Hive.box('chatHistoryL');
  }

  Box getAllMessages() {
    return chatHistory;
  }

  void insert(Message pMess) async {
    await chatHistory.add(pMess);
    print(chatHistory.values);
  }

  void deleteAll() async {
    Hive.box('chatHistoryL').clear();
    Hive.box('chatHistoryL').close();
    //chatHistory.close();
  }
}
