//Clase que modela la info de un usuario
class UserAuthModel {
  String nombres;
  String apellidos;
  String email;
  String password;
  String secondPassword;

  UserAuthModel();

  UserAuthModel.fromValues(
      {this.nombres,
      this.apellidos,
      this.email,
      this.password,
      this.secondPassword});

  getFullName() {
    return {"name": this.nombres, "lastname": this.apellidos};
  }

  bool validatePassword() {
    return this.password == this.secondPassword;
  }
}
