import 'package:audioplayers/audioplayers.dart';

enum PlayerState { stopped, playing, paused }
enum PlayingRouteState { speakers, earpiece }

class PlayerModel {
  final String audioPath;

  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Duration _duration = Duration(seconds: 0);
  Duration _position = Duration(seconds: 0);
  PlayerState _playerState = PlayerState.stopped;

  PlayerModel({this.audioPath});

  PlayerState get playerState => _playerState;

  set playerState(PlayerState value) {
    _playerState = value;
  }

  Duration get position => _position;

  set position(Duration value) {
    _position = value;
  }

  Duration get duration => _duration;

  set duration(Duration value) {
    _duration = value;
  }

  AudioPlayerState get audioPlayerState => _audioPlayerState;

  set audioPlayerState(AudioPlayerState value) {
    _audioPlayerState = value;
  }

  AudioPlayer get audioPlayer => _audioPlayer;

  set audioPlayer(AudioPlayer value) {
    _audioPlayer = value;
  }

  get isPlaying => _playerState == PlayerState.playing;

  get isPaused => _playerState == PlayerState.paused;

  get isStopped => _playerState == PlayerState.stopped;

  get durationText => _duration.toString().split('.').first ?? '';

  get positionText => _position.toString().split('.').first ?? '';

  get sliderValue => (this._position != null &&
          this._duration != null &&
          this._position.inMilliseconds > 0 &&
          this._position.inMilliseconds < this._duration.inMilliseconds)
      ? this._position.inMilliseconds / this._duration.inMilliseconds
      : 0.0;
  get sliderPositionText => this._position != null
      ? '${this.positionText ?? ''} ' ''
      : (this._duration != null ? this.durationText : '');

  get sliderDurationText => this._position != null
      ? '${this.durationText ?? ''}'
      : (this._duration != null ? this.durationText : '');
}
