class MeditationRoute {
  String name;
  String path;

  MeditationRoute(String pName, String pPath) {
    this.name = pName;
    this.path = pPath;
  }

  void updatePath(String pNew) {
    path = pNew;
  }

  String retrievePath() {
    return path;
  }

  String retrieveName() {
    return name;
  }
}
