// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ProfileInformationAdapter extends TypeAdapter<ProfileInformation> {
  @override
  final int typeId = 2;

  @override
  ProfileInformation read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ProfileInformation(
      fields[0] as String,
      fields[1] as int,
      fields[2] as int,
      fields[3] as int,
      fields[4] as int,
      fields[5] as int,
      fields[6] as int,
    );
  }

  @override
  void write(BinaryWriter writer, ProfileInformation obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.uId)
      ..writeByte(1)
      ..write(obj.sessions)
      ..writeByte(2)
      ..write(obj.time)
      ..writeByte(3)
      ..write(obj.positive)
      ..writeByte(4)
      ..write(obj.negative)
      ..writeByte(5)
      ..write(obj.messages)
      ..writeByte(6)
      ..write(obj.createdM);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProfileInformationAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
