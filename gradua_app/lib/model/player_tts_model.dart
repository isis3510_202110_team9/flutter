import 'dart:io' show Platform;

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_tts/flutter_tts.dart';

enum PlayerTTState { stopped, playing, paused, continued }
enum PlayingRouteState { speakers, earpiece }

class PlayerTTSModel {
  String text;
  FlutterTts _ttsPlayer;
  String currentWord;

  // AudioPlayer _audioPlayer;
  // AudioPlayerState _audioPlayerState;
  // Duration _duration = Duration(seconds: 0);
  // Duration _position = Duration(seconds: 0);
  PlayerTTState _playerState = PlayerTTState.stopped;

  PlayerTTSModel({this.text}) {
    this._ttsPlayer = FlutterTts();
    this.currentWord = '';
  }

  PlayerTTState get playerState => _playerState;

  bool get isIOS => !kIsWeb && Platform.isIOS;
  bool get isAndroid => !kIsWeb && Platform.isAndroid;
  bool get isWeb => kIsWeb;

  set playerState(PlayerTTState value) {
    _playerState = value;
  }

  set word(String word) {
    this.currentWord = word;
  }

  Future getDefaultEngine() async {
    var engine = await _ttsPlayer.getDefaultEngine;
    if (engine != null) {
      print(engine);
    }
  }

  // Duration get position => _position;

  // set position(Duration value) {
  //   _position = value;
  // }
  //
  // Duration get duration => _duration;
  //
  // set duration(Duration value) {
  //   _duration = value;
  // }
  //
  // AudioPlayerState get audioPlayerState => _audioPlayerState;
  //
  // set audioPlayerState(AudioPlayerState value) {
  //   _audioPlayerState = value;
  // }
  //
  FlutterTts get audioPlayer => this._ttsPlayer;
  //
  // set audioPlayer(AudioPlayer value) {
  //   _audioPlayer = value;
  // }

  get isPlaying => _playerState == PlayerTTState.playing;

  get isPaused => _playerState == PlayerTTState.paused;

  get isStopped => _playerState == PlayerTTState.stopped;

  get isContinued => _playerState == PlayerTTState.continued;
  //
  // get durationText => _duration.toString().split('.').first ?? '';
  //
  // get positionText => _position.toString().split('.').first ?? '';
  //
  // get sliderValue => (this._position != null &&
  //     this._duration != null &&
  //     this._position.inMilliseconds > 0 &&
  //     this._position.inMilliseconds < this._duration.inMilliseconds)
  //     ? this._position.inMilliseconds / this._duration.inMilliseconds
  //     : 0.0;
  // get sliderPositionText => this._position != null
  //     ? '${this.positionText ?? ''} ' ''
  //     : (this._duration != null ? this.durationText : '');
  //
  // get sliderDurationText => this._position != null
  //     ? '${this.durationText ?? ''}'
  //     : (this._duration != null ? this.durationText : '');
}
