import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

class ContentProvider {
  Future<String> downloadFile(String url, String fileName) async {
    print('Entra a descargar');
    final directory = await getApplicationDocumentsDirectory();
    String dir = directory.path;
    print('App directory: $dir');
    HttpClient httpClient = new HttpClient();
    File file;
    String filePath = '';
    String myUrl = '';

    try {
      myUrl = url;
      var request = await httpClient.getUrl(Uri.parse(myUrl));
      print('Request: ${request.toString()}');
      var response = await request.close();
      if (response.statusCode == 200) {
        var bytes = await consolidateHttpClientResponseBytes(response);
        filePath = p.join(dir, fileName);
        print('File path: $filePath');
        file = File(filePath);
        await file.writeAsBytes(bytes);
        print('File succesfully saved');
      } else {
        filePath = 'Error code: ' + response.statusCode.toString();
        print('Error: $filePath');
      }
    } catch (ex) {
      filePath = 'Can not fetch url';
    }

    return filePath;
  }

  Future<void> removeFileFromAppDocuments(String fileName) async {
    final directory = await getApplicationDocumentsDirectory();
    String dir = directory.path;
    String path = p.join(dir, fileName);
    final File toRemove = File(path);
    try {
      await toRemove.delete();
      print('File: $fileName succesfully removed from: $dir');
    } catch (e) {
      print('Error removing file from: $path');
      print(e.toString());
    }
  }
}
