import 'dart:async'; //For StreamController/Stream
import 'dart:io'; //InternetAddress utility

import 'package:connectivity/connectivity.dart';

//The following code implements a Singleton for the views
// to subscribe to and listen to changes in connectivity
class ConnectionStatusService {
  //Code taken from: https://stackoverflow.com/questions/49648022/check-whether-there-is-an-internet-connection-available-on-flutter-app

  //This creates the single instance by calling the `_internal` constructor specified below
  static final ConnectionStatusService _singleton =
      new ConnectionStatusService._internal();
  ConnectionStatusService._internal();

  //This is what's used to retrieve the instance through the app
  static ConnectionStatusService getInstance() => _singleton;

  //This tracks the current connection status
  bool hasConnection = false;

  //This is how we'll allow subscribing to connection changes
  StreamController<bool> connectionChangeController =
      new StreamController.broadcast();

  //flutter_connectivity
  final Connectivity _connectivity = Connectivity();

  //Hook into flutter_connectivity's Stream to listen for changes
  //And check the connection status out of the gate
  void initialize() {
    _connectivity.onConnectivityChanged.listen(_connectionChange);
    connectionChangeController.stream.listen((event) {
      print("Al menos acá entra");
    });
    checkConnection(ConnectivityResult.wifi);
  }

  Stream<bool> get connectionChange => connectionChangeController.stream;

  //This method can be used to check in the moment if the phones has Connectivity
  Future<bool> get hasConnectionNow async {
    ConnectivityResult result = await this._connectivity.checkConnectivity();
    print('Result of connection check was: ${result.toString()}');
    return checkConnection(result);
  }

  //A clean up method to close our StreamController
  //   Because this is meant to exist through the entire application life cycle this isn't
  //   really an issue
  void dispose() {
    connectionChangeController.close();
  }

  //flutter_connectivity's listener
  void _connectionChange(ConnectivityResult result) {
    print("Cambiazo papá - Connectivity Result");
    print(result);
    checkConnection(result);
  }

  //The test to actually see if there is a connection
  Future<bool> checkConnection(ConnectivityResult resultConnect) async {
    bool previousConnection = hasConnection;

    try {
      print('google lookup');
      final result = await InternetAddress.lookup('google.com');
      print(result[0].rawAddress);
      if (result.isNotEmpty &&
          result[0].rawAddress.isNotEmpty &&
          resultConnect != ConnectivityResult.none) {
        hasConnection = true;
      } else {
        hasConnection = false;
      }
    } on SocketException catch (_) {
      hasConnection = false;
    }

    //The connection status changed send out an update to all listeners
    print("Checking if prev is differento to actual");
    print("prev: " + previousConnection.toString());
    print("Now: " + hasConnection.toString());

    if (previousConnection != hasConnection) {
      print("Indeed prev was different from actual");
      connectionChangeController.add(hasConnection);
    }

    return hasConnection;
  }
}
