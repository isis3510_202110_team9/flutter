import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future setUserProperties({@required String uid}) async {
    await _analytics.setUserId(uid);
    //The following is for keeping more user data
    // await _analytics.setUserProperty(name: name, value: value)
  }

  Future logCustomEvent(
      {@required String name, Map<String, Object> parameters}) async {
    await _analytics.logEvent(name: name, parameters: parameters);
  }

  Future logLogIn() async {
    await _analytics.logLogin(loginMethod: "email");
  }

  Future LogAddToFavorites() async {
    _analytics.logEvent(name: 'savedAsFavorite');
  }

  Future LogRemoveFromFavorites() async {
    _analytics.logEvent(name: 'removedFromFavorite');
  }

  Future downloadMeditation() async {
    _analytics.logEvent(name: 'downloadMeditation');
  }

  Future deleteDownloadedMeditation() async {
    _analytics.logEvent(name: 'deleteDownloadedMeditation');
  }

  Future logFavorites() async {
    _analytics.logEvent(name: 'playFavoritesMeditation');
  }

  Future logPopular() async {
    _analytics.logEvent(name: 'playPopularMeditation');
  }
}
