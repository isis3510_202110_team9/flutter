import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gradua_app/model/user_auth_model.dart';

class DbManager {
  // -------
  // CONSTS
  // -------

  /* Allows this class be a singleton */
  static final DbManager _dbManager = DbManager._internal();

  /* Users collection reference */
  final CollectionReference _usersCol =
      FirebaseFirestore.instance.collection("users");

  /* Users collection reference */
  final CollectionReference _meditationsCol =
      FirebaseFirestore.instance.collection("meditations");

  // --------
  // METHODS
  // --------
  factory DbManager() {
    return _dbManager;
  }

  DbManager._internal();

  Future createUser(uid, UserAuthModel user) async {
    return await _usersCol
        .doc(uid)
        .set({...user.getFullName(), "favorites": []});
  }

  Future getUser(uid) async {
    var user = await _usersCol.doc(uid).get();
    print('User from db: ${user.data()['name']}');
    return user;
  }

  Future persistInterests(uid, interests) async {
    return await _usersCol.doc(uid).update({'interests': interests});
  }

  Future incrementMeditationReproductions(meditationId) async {
    var meditation = await _meditationsCol.doc(meditationId).get();

    int currentReproductionsValue = meditation.get("reproductions");

    return await _meditationsCol
        .doc(meditationId)
        .update({'reproductions': currentReproductionsValue + 1});
  }

  Future<void> addMeditationToFavorites(
      String uid, String meditationName) async {
    var user = await _usersCol.doc(uid).get();
    var favoritos = [];

    try {
      //Tries to obtain the favorites list from de DB
      var dbList = user.data()['favorites'];
      print('Favoritos obtenido: ' + dbList.toString());
      if (dbList != null && !dbList.contains(meditationName)) {
        favoritos = dbList;
        favoritos.add(meditationName);
        print('Arreglo favoritos no contiene la meditación');
      } else if (dbList == null) {
        favoritos.add(meditationName);
      } else if (dbList != null) {
        favoritos = dbList;
      }
    } catch (e) {
      print('Error retrieving favorites list of user: $uid');
      print(e.toString());
    }

    await _usersCol
        .doc(uid)
        .update({'favorites': favoritos})
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  Future<void> removeMeditationFromFavorites(
      String uid, String meditationName) async {
    _usersCol
        .doc(uid)
        .update({
          'favorites': FieldValue.arrayRemove([meditationName])
        })
        .then((value) => print("User's Property Deleted"))
        .catchError(
            (error) => print("Failed to delete user's property: $error"));
  }

  getAllMeditations() async {
    QuerySnapshot querySnapshot = await _meditationsCol.get();

    var response = [];

    querySnapshot.docs.forEach((doc) {
      response.add(doc.data());
    });

    return (response);
  }

  getPopularMeditations() async {
    QuerySnapshot querySnapshot = await _meditationsCol
        .orderBy("reproductions", descending: true)
        .limit(3)
        .get();

    var response = [];

    querySnapshot.docs.forEach((doc) {
      response.add(doc.data());
    });

    return (response);
  }

  getFavoritesMeditationsId(uid) async {
    var user = await _usersCol.doc(uid).get();
    return (user.data()['favorites']);
  }
}
