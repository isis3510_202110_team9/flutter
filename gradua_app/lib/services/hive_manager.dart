import 'dart:io';

import 'package:gradua_app/model/profile_model.dart';
import 'package:gradua_app/screens/profile.dart';
import 'package:hive/hive.dart';

class HiveManager {
  Box preferences;
  Box favorites;
  Box profile;
  Box profileInfo;

  static final HiveManager _singleton = new HiveManager._internal();

  HiveManager._internal();

  static HiveManager getInstance() => _singleton;

  Future initialize() async {
    print("Se abre la box de preferencias preferencias y favoritos");
    await Hive.openBox('preferences');
    await Hive.openBox('favorites');
    await Hive.openBox('profile');
    await Hive.openBox('profileInfo');
    await Hive.openBox('chatHistoryL');
    //{nombreMeditacion: ruta, nombremeti: ruta, ....}.toMap()
    print("Se leen las preferencias");
    preferences = Hive.box('preferences');
    favorites = Hive.box('favorites');
    profile = Hive.box('profile');
    profileInfo = Hive.box('profileInfo');
  }

  void initializeProfileInfo(String uId) {
    if (profileInfo.get(uId) == null) {
      insertInitialInformation(uId.toString(), 0, 0, 0, 0, 0, 0);
    }
  }

  Box getAllPreferences() {
    return Hive.box('preferences');
  }

  String getLocalPathIfExist(String meditationId) {
    return Hive.box('preferences').get(meditationId.toLowerCase());
  }

  bool checkMeditationFileExists(String meditationId) {
    if (meditationId == null) return false;
    String localPath = this.getLocalPathIfExist(meditationId.toLowerCase());
    if (localPath == null) return false;
    bool fileExists = File(localPath).existsSync();
    return fileExists;
  }

  Box getProfileInformationBox() {
    return Hive.box('profileInfo');
  }

  ProfileInformation getProfInfo(String uId) {
    return profileInfo.get(uId);
  }

  void updateMessagesCount(String uid, int totalMes) async {
    ProfileInformation actual = getProfInfo(uid);
    await profileInfo.put(
        uid,
        ProfileInformation(actual.uId, actual.sessions, actual.time,
            actual.positive, actual.negative, totalMes, actual.createdM));
  }

  void updatePositiveEmotions(String uid) async {
    ProfileInformation actual = getProfInfo(uid);
    int current = (actual.positive + 1);
    await profileInfo.put(
        uid,
        ProfileInformation(actual.uId, actual.sessions, actual.time, current,
            actual.negative, actual.messages, actual.createdM));
  }

  void updateCreatedCounter(String uid) async {
    ProfileInformation actual = getProfInfo(uid);
    int current = (actual.createdM + 1);
    await profileInfo.put(
        uid,
        ProfileInformation(actual.uId, actual.sessions, actual.time,
            actual.positive, actual.negative, actual.messages, current));
  }

  void updateNegativeEmotions(String uid) async {
    ProfileInformation actual = getProfInfo(uid);
    int current = (actual.negative + 1);
    await profileInfo.put(
        uid,
        ProfileInformation(actual.uId, actual.sessions, actual.time,
            actual.positive, current, actual.messages, actual.createdM));
  }

  void updateTime(String uid, int time) async {
    ProfileInformation actual = getProfInfo(uid);
    int sessions = (actual.sessions + 1);
    int timeCurrent = actual.time;
    await profileInfo.put(
        uid,
        ProfileInformation(
            actual.uId,
            sessions,
            (time + timeCurrent),
            actual.positive,
            actual.negative,
            actual.messages,
            actual.createdM));
  }

  void insertInitialInformation(String id, int sessions, int time, int positive,
      int negative, int messages, int created) {
    profileInfo.put(
        id,
        ProfileInformation(
            id, sessions, time, positive, negative, messages, created));
  }

  void insertRoute(String meditationId, String route) async {
    // Inserts a route to the preferences box if it does not exist
    if (meditationId != null) {
      preferences.put(meditationId.toLowerCase(), route);
    }
  }

  void removeRoute(String meditationId) async {
    if (meditationId != null) preferences.delete(meditationId.toLowerCase());
  }

  Future<List<dynamic>> getAllFavorites() async {
    // Return de list of all favorite mediations
    return favorites.keys.toList();
  }

  void insertFavorite(String meditationId) async {
    // Inserts to favorites a fiven meditation if it does not exists
    if (meditationId != null &&
        !favorites.containsKey(meditationId.toLowerCase())) {
      favorites.put(meditationId.toLowerCase(), meditationId.toLowerCase());
    }
  }

  void insertAllFavorites(List<String> favoritas) async {
    for (String favorita in favoritas) {
      try {
        await favorites.put(favorita.toLowerCase(), favorita.toLowerCase());
      } catch (e) {
        print('Excepción insertando Favorita en el arreglo de favoritas');
        print(e.toString());
      }
    }
  }

  void deleteFavorite(String meditationId) async {
    // Remove a given meditation from favorites if it exists
    if (meditationId != null &&
        favorites.containsKey(meditationId.toLowerCase())) {
      favorites.delete(meditationId.toLowerCase());
    }
  }

  bool amIFavorite(String meditationId) {
    //  Checks if a given meditation is a favorite meditation
    if (meditationId == null) return false;
    if (favorites.containsKey(meditationId.toLowerCase())) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> insertNames(String name, {String lastName = ''}) {
    print('Inserting names: $name');
    profile.put('name', name);
    profile.put('lastname', lastName);
  }

  Map<String, String> getNames() {
    try {
      String name = profile.get('name');
      String lastName = profile.get('lastname');
      return {'name': name, 'lastname': lastName};
    } catch (e) {
      print('Excepción obteniendo perfil desde Hive');
      print(e.toString());
      return {'name': '', 'lastname': ''};
    }
  }

  void deleteAll() async {
    Hive.box('preferences').clear();
    Hive.box('favorites').clear();
    Hive.box('profile').clear();
  }
}
