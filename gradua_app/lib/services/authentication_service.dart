import 'package:firebase_auth/firebase_auth.dart';

enum AuthResult { SignedIn, SignedUp, SignedOut, Error }

class SignUpResult {
  AuthResult status;
  UserCredential credentials;
  SignUpResult(this.status, this.credentials);
}

class AuthenticationService {
  final FirebaseAuth _firebaseAuth;

  AuthenticationService(this._firebaseAuth);

  Stream<User> get authStateChanges => _firebaseAuth.authStateChanges();

  Future<String> get name async {
    try {
      String name = _firebaseAuth.currentUser.displayName;
      print('Username: $name');
      return name;
    } on FirebaseException catch (e) {
      print(e.message);
      return e.message;
    }
  }

  Future<AuthResult> signOut() async {
    try {
      await _firebaseAuth.signOut();
      return AuthResult.SignedOut;
    } on FirebaseException catch (e) {
      print(e.message);
      return AuthResult.Error;
    }
  }

  Future<AuthResult> signIn({String email, String password}) async {
    try {
      print(await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password));
      return AuthResult.SignedIn;
    } on FirebaseAuthException catch (e) {
      print(e.message);
      return AuthResult.Error;
    }
  }

  Future<SignUpResult> signUp({String email, String password}) async {
    try {
      UserCredential credentials = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      return SignUpResult(AuthResult.SignedUp, credentials);
    } on FirebaseAuthException catch (e) {
      print(e.message);
      return SignUpResult(AuthResult.Error, null);
    }
  }
}
