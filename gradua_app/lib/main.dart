import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/model/preferences_model.dart';
import 'package:gradua_app/model/profile_model.dart';
import 'package:gradua_app/router.dart';
import 'package:gradua_app/screens/home.dart';
import 'package:gradua_app/screens/welcome.dart';
import 'package:gradua_app/services/analytics_service.dart';
import 'package:gradua_app/services/authentication_service.dart';
import 'package:gradua_app/services/connection_status_service.dart';
import 'package:gradua_app/services/hive_manager.dart';
import 'package:gradua_app/services/navigation_service.dart';
import 'package:gradua_app/styles/custom_theme.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import 'model/message_model.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  var path = await getApplicationDocumentsDirectory();
  Hive.init(path.path);
  Hive.registerAdapter(MessageAdapter());
  Hive.registerAdapter(PreferencesAdapter());
  Hive.registerAdapter(ProfileInformationAdapter());

  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  // Acá se inician los Singletons implementados usando
  // el contructor ._internal().

  ConnectionStatusService connectionStatus =
      ConnectionStatusService.getInstance();
  connectionStatus.initialize();
  HiveManager hive = HiveManager.getInstance();
  hive.initialize();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<AuthenticationService>(
          create: (_) => AuthenticationService(FirebaseAuth.instance),
        ),
        Provider<AnalyticsService>(create: (_) => AnalyticsService()),
        Provider<NavigationService>(create: (_) => NavigationService()),
        StreamProvider(
            create: (context) =>
                context.read<AuthenticationService>().authStateChanges),
        // StreamProvider(
        //     create: (context) =>
        //         ConnectionStatusService.getInstance().connectionChange),
      ],
      builder: (context, widget) {
        return MaterialApp(
          title: "Gradua",
          theme: CustomTheme.getTheme(context),
          navigatorKey: context.read<NavigationService>().navigationKey,
          onGenerateRoute: generateRoute,
          home: AuthenticationWrapper(),
          initialRoute: '/',
          navigatorObservers: [
            context.read<AnalyticsService>().getAnalyticsObserver()
          ],
        );
      },
    );
  }
}

void flushNavigator(BuildContext context) {
  Navigator.popUntil(context, ModalRoute.withName('/'));
}

class AuthenticationWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final firebaseuser = context.watch<User>();
    print("Build main wrapper");

    if (firebaseuser != null) {
      print("Home page");
      return Home();
    } else {
      print("Welcome");
      return WelcomeScreen();
    }
  }
}
