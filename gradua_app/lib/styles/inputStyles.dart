import 'package:flutter/material.dart';

// InputDecoration authInputs = InputDecoration (hint) => return InputDecoration(
//     filled: true,
//     fillColor: Color.fromRGBO(248, 229, 255, 1),
//     hoverColor: Color.fromRGBO(248, 229, 255, 1),
//     border: UnderlineInputBorder(
//         borderRadius: BorderRadius.circular(10), borderSide: BorderSide.none),
//     hintText: hint);

InputDecoration authInputs(String hint) {
  return InputDecoration(
      filled: true,
      fillColor: Color.fromRGBO(94, 96, 206, 0.12),
      hoverColor: Color.fromRGBO(94, 96, 206, 0.12),
      border: UnderlineInputBorder(
          borderRadius: BorderRadius.circular(10), borderSide: BorderSide.none),
      hintText: hint);
}
