import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class CustomTheme {
  static final graduaDarkPurple = Color(0xFF7400B8);
  static final graduaPurple = Color(0xFF5E60CE);
  static final graduaBlue = Color(0xFF4EA8DE);
  static final graduaLightBlue = Color(0xFF48BFE3);
  static final graduaTeal = Color(0xFF64D9D6);

  static final backGroundColor = Color(0xFFF5F5F5);

  static final backGroundTextColor = Colors.black;
  static final purpleTextColor = Colors.white;
  static final blueTextColor = Color(0xAE000000);
  static final tealTextColor = Colors.black;
  static final dividerColor = Color(0xFFC1C3CB);

  static final mainGradient =
      RadialGradient(center: Alignment.center, radius: 2, colors: [
    HSLColor.fromColor(CustomTheme.graduaPurple)
        .withSaturation(0.8)
        .toColor(), //Gradua pruple more saturated
    CustomTheme.graduaPurple,
    Colors.white,
  ], stops: [
    0.02,
    0.15,
    0.7
  ]);

  static final secondaryGradient =
      RadialGradient(center: Alignment.center, radius: 0.4, colors: [
    HSLColor.fromColor(CustomTheme.graduaLightBlue)
        .withLightness(0.7)
        .toColor(),
    HSLColor.fromColor(CustomTheme.graduaLightBlue)
        .withLightness(0.8)
        .toColor(),
    Colors.white,
  ], stops: [
    0.01,
    0.3,
    0.9
  ]);

  static final playerBlueGradient =
      RadialGradient(center: Alignment.bottomLeft, radius: 2, colors: [
    Color(0xFFA8DEFF),
    Color(0xFFDAF1FF),
    Colors.white,
  ], stops: [
    0.02,
    0.5,
    1
  ]);

  static final mainShadow = BoxShadow(
    color: Colors.grey.withOpacity(0.5),
    spreadRadius: 5,
    blurRadius: 7,
    offset: Offset(0, 3), // changes position of shadow
  );

  static final playerSliderTextStyle = TextStyle(
    fontSize: 14.0,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
    color: Colors.blueGrey,
  );

  static ThemeData getTheme(BuildContext context) {
    double minWidth = 100;
    return Theme.of(context).copyWith(
      primaryColor: CustomTheme.graduaDarkPurple,
      primaryColorDark: CustomTheme.graduaDarkPurple,
      accentColor: CustomTheme.graduaTeal,
      backgroundColor: CustomTheme.backGroundColor,
      canvasColor: Colors.white,
      cardColor: CustomTheme.backGroundColor,

      dividerColor: dividerColor, //Same hue as graduaBlue.

      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        elevation: 30,
      ),

      inputDecorationTheme: InputDecorationTheme(
        floatingLabelBehavior: FloatingLabelBehavior.always,
        filled: false,
        labelStyle: TextStyle(
          color: CustomTheme.graduaDarkPurple,
          fontFamily: "Roboto",
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
        enabledBorder: OutlineInputBorder(
          gapPadding: 10,
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(
            color: CustomTheme.dividerColor,
            width: 2,
            style: BorderStyle.solid,
          ),
        ),
        errorBorder: OutlineInputBorder(
          gapPadding: 10,
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: Colors.redAccent,
            width: 3,
            style: BorderStyle.solid,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: BorderSide(
            width: 3,
            style: BorderStyle.solid,
          ),
        ),
        prefixStyle: TextStyle(fontSize: 10),
      ),

      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(
            CustomTheme.purpleTextColor,
          ),
          padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
            EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          ),
          textStyle: MaterialStateProperty.all<TextStyle>(
            TextStyle(
              fontSize: 20,
              fontFamily: 'Roboto',
            ),
          ),
          minimumSize: MaterialStateProperty.all<Size>(
            Size(minWidth, 60),
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
          ),
          backgroundColor: MaterialStateColor.resolveWith(
            (states) => CustomTheme.graduaPurple,
          ),
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(
            CustomTheme.graduaPurple,
          ),
          padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
            EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          ),
          textStyle: MaterialStateProperty.all<TextStyle>(
            TextStyle(
              fontSize: 20,
              fontFamily: 'Roboto',
              fontWeight: FontWeight.w500,
            ),
          ),
          minimumSize: MaterialStateProperty.all<Size>(
            Size(minWidth, 60),
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
          ),
          side: MaterialStateProperty.all<BorderSide>(
            BorderSide(
              width: 2,
              style: BorderStyle.solid,
              color: CustomTheme.graduaPurple,
            ),
          ),
        ),
      ),
    );
  }
}
