import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final RadialGradient SecondaryGradient =
    RadialGradient(center: Alignment.topRight, radius: 2, colors: [
  Color.fromRGBO(139, 43, 196, 0.01),
  Color.fromRGBO(139, 43, 196, 0.001),
  Colors.white,
], stops: [
  0.02,
  0.5,
  1
]);

final RadialGradient MainGradient =
    RadialGradient(center: Alignment.center, radius: 2.7, colors: [
  Color.fromRGBO(94, 96, 206, 1),
  Color.fromRGBO(94, 96, 206, 0.0),
  Colors.white,
], stops: [
  0.02,
  0.5,
  1
]);

final RadialGradient BlueGradient =
    RadialGradient(center: Alignment.center, radius: 0.4, colors: [
  Color(0xFFA7E9FA),
  Color(0xFFF2F9FF),
  Colors.white,
], stops: [
  0.01,
  0.4,
  1
]);
