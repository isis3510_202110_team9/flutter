import 'package:flutter/material.dart';

final ButtonStyle PurpleOutlinedButton = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(
                width: 3,
                style: BorderStyle.solid,
                color: Color.fromRGBO(126, 128, 216, 1)))));

final ButtonStyle PurpleElevatedButton = ButtonStyle(
    backgroundColor:
        MaterialStateProperty.all<Color>(Color.fromRGBO(126, 128, 216, 1)),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.transparent))));
