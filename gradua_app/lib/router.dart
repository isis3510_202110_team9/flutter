import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/screens/goals.dart';
import 'package:gradua_app/screens/player.dart';
import 'package:gradua_app/screens/player_tts.dart';
import 'package:gradua_app/screens/sign_in.dart';
import 'package:gradua_app/screens/sign_up.dart';
import 'package:gradua_app/screens/welcome.dart';

const String WelcomeViewRoute = "Welcome";
const String SignInViewRoute = "SignIn";
const String SignUpViewRoute = "SignUp";
const String GoalsViewRoute = "Goals";
const String HomeViewRoute = "Home";
const String GraduaViewRoute = "Gradua";
const String ProfileViewRoute = "Profile";
const String PlayerViewRoute = "Player";
const String PlayerTTSViewRoute = "PlayerTTS";

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case WelcomeViewRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: WelcomeScreen(),
      );
    case SignInViewRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: SignIn(),
      );
    case SignUpViewRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: SignUp(),
      );
    case GoalsViewRoute:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: SignInGoals(
          uuid: settings.arguments,
        ),
      );
    case PlayerViewRoute:
      Map _arguments = settings.arguments as Map;
      print('ARGUMENTS: ${_arguments.toString()}');
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: Player(
          meditationId: _arguments['label'],
          audioPath: _arguments['link'],
        ),
      );
    case PlayerTTSViewRoute:
      Map _arguments = settings.arguments as Map;
      print('ARGUMENTS: ${_arguments.toString()}');
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: PlayerTTS(
          text: _arguments['text'],
        ),
      );

    default:
      return _getPageRoute(
        routeName: settings.name,
        viewToShow: WelcomeScreen(),
      );
  }
}

PageRoute _getPageRoute({String routeName, Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (_) => viewToShow);
}
