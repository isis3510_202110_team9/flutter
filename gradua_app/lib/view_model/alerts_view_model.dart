import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradua_app/services/connection_status_service.dart';

class AlertsViewModel {
  bool _hasConnection = true;

  BuildContext _buildContext;

  AlertsViewModel(this._buildContext);

  void subscribeConnection(void Function(bool) callback) {
    ConnectionStatusService.getInstance().connectionChange.listen(callback);
  }

  void activateConnectionAlert(Widget snackBarLost, Widget snackBarRestored) {
    void checkChangedConnection(bool hasConnection) {
      print('Calles callback at connection:' + hasConnection.toString());
      this._hasConnection = hasConnection;
      if (!this._hasConnection) {
        print('It didnt have connection');
        ScaffoldMessenger.of(this._buildContext).removeCurrentSnackBar();
        ScaffoldMessenger.of(this._buildContext).showSnackBar(snackBarLost);
        print('Snackbar shown');
        print('Context');
        print(this._buildContext);
      } else {
        print('removiendo snackbars de alerta');
        ScaffoldMessenger.of(this._buildContext).removeCurrentSnackBar();
        ScaffoldMessenger.of(this._buildContext).showSnackBar(snackBarRestored);
      }
    }

    subscribeConnection(checkChangedConnection);
  }
}
