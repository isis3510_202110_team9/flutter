import 'package:flutter/cupertino.dart';
import 'package:gradua_app/model/goals_model.dart';

class GoalsViewModel extends ChangeNotifier {
  List<GoalModel> goals;

  GoalsViewModel() {
    goals = [
      GoalModel("Relaxation", CupertinoIcons.headphones),
      GoalModel("Insomnia  ", CupertinoIcons.moon_zzz_fill),
      GoalModel("Focus", CupertinoIcons.eyeglasses),
      GoalModel("Stress", CupertinoIcons.sparkles),
      GoalModel("Nature", CupertinoIcons.tree),
      GoalModel("Daily", CupertinoIcons.calendar_circle_fill),
      GoalModel("For kids", CupertinoIcons.game_controller_solid),
      GoalModel("Others", CupertinoIcons.scribble)
    ];
    notifyListeners();
  }

  List<String> get goalsNames {
    return goals.map((e) => e.name).toList();
  }

  List<IconData> get goalsIcons {
    return goals.map((e) => e.icon).toList();
  }

  List<bool> get selected {
    return goals.map((e) => e.isSelected).toList();
  }

  String getName(int index) {
    return this.goals[index].name;
  }

  IconData getIcon(int index) {
    return this.goals[index].icon;
  }

  bool getSelection(int index) {
    return this.goals[index].isSelected;
  }

  void changeSelection(int index) {
    goals[index].changeSelection();
    notifyListeners();
  }
}
