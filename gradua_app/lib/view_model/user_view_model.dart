import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:gradua_app/model/user_auth_model.dart';

class UserAuthViewModel {
  final UserAuthModel _user = UserAuthModel();

  final TextEditingController ctlNombres = TextEditingController();
  final TextEditingController ctlApellidos = TextEditingController();
  final TextEditingController ctlEmail = TextEditingController();
  final TextEditingController ctlPassword = TextEditingController();
  final TextEditingController ctlSecondPassword = TextEditingController();

  UserAuthModel get user => _user;

  String get nombres => ctlNombres.text;

  String get apellidos => ctlApellidos.text;

  String get email => ctlEmail.text;

  String get password => ctlPassword.text;

  String get secondPassword => ctlSecondPassword.text;

  UserAuthViewModel() {
    ctlNombres.addListener(() => _user.nombres = ctlNombres.text);
    ctlApellidos.addListener(() => _user.apellidos = ctlApellidos.text);
    ctlEmail.addListener(() => _user.email = ctlEmail.text);
    ctlPassword.addListener(() => _user.password = ctlPassword.text);
    ctlSecondPassword
        .addListener(() => _user.secondPassword = ctlSecondPassword.text);
  }

  void dispose() {
    this.ctlNombres.dispose();
    this.ctlApellidos.dispose();
    this.ctlEmail.dispose();
    this.ctlPassword.dispose();
    this.ctlSecondPassword.dispose();
  }

  bool validatePassword() {
    return ctlPassword.text == ctlSecondPassword.text;
  }

  String Function(String) buildEmailValidator(String errorMsg) => (value) =>
      EmailValidator.validate(value) ? null : ("Input a valid email");

  String Function(String) buildNamesValidator(String errorMsg) {
    // ignore: missing_return
    return (value) {
      if (value.isEmpty) {
        return errorMsg;
      }
    };
  }

  String Function(String) buildSecondPswdValidator(String errorMsg) {
    // ignore: missing_return
    return (value) {
      if (this.password != this.secondPassword) {
        return errorMsg;
      }
    };
  }
}
