import 'package:hive/hive.dart';

class ProfilePreferences {
  Box preferences;
  Future initDB() async {
    await Hive.openBox('preferences');
    //{nombreMeditacion: ruta, nombremeti: ruta, ....}.toMap()
    print("Entró a iniciar eso");
    preferences = Hive.box('preferences');
  }

  Box getAllPreferences() {
    return Hive.box('preferences');
  }

  String getLocalPathIfExist(meditationId) {
    print("Va a buscar $meditationId");
    Hive.openBox('preferences');
    return Hive.box('preferences').get(meditationId);
  }

  // insertPreference(Preferences pPref) async {
  //   await preferences.add(preferences);
  //   print(preferences.values);
  // }

  insertFavorite(String name, String route) async {
    preferences.put(name, route);
  }

  removeFavorite(String name) async {
    preferences.delete(name);
  }

  void deleteAll() async {
    Hive.box('preferences').clear();
    Hive.box('preferences').close();
    //chatHistory.close();
  }
}
