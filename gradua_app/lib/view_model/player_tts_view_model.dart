import 'dart:async';

import 'package:gradua_app/model/player_tts_model.dart';

class PlayerTTSViewModel {
  PlayerTTSModel _player;
  // ProfilePreferences preferences = ProfilePreferences();

  StreamController<PlayerTTSModel> _streamController =
      StreamController.broadcast();

  PlayerTTSViewModel(String text) {
    this._player = PlayerTTSModel(text: text
        // '/data/user/0/com.gradua.gradua/app_flutter/Concentration'
        );
    print('Model');
    _initAudioPlayer();
  }

  Stream<PlayerTTSModel> get playerStream => _streamController.stream;

  void changeText(String text) {
    _player.text = text;
    _streamController.add(_player);
  }

  void _initAudioPlayer() {
    // flutterTts = this._player.audioPlayer;

    _player.audioPlayer.setLanguage("es-US");
    _player.audioPlayer.getLanguages.then((value) => print(value.toString()));

    if (_player.isAndroid) {
      _player.getDefaultEngine();
    }

    _player.audioPlayer.setStartHandler(() {
      print("Playing");
      _player.playerState = PlayerTTState.playing;
      _streamController.add(_player);
      // ttsState = TtsState.playing;
    });

    _player.audioPlayer.setCompletionHandler(() {
      print("Complete");
      _player.playerState = PlayerTTState.stopped;
      _streamController.add(_player);
    });

    _player.audioPlayer.setCancelHandler(() {
      print("Cancel");
      _player.playerState = PlayerTTState.stopped;
      _streamController.add(_player);
    });

    if (_player.isWeb || _player.isIOS) {
      _player.audioPlayer.setPauseHandler(() {
        print("Paused");
        _player.playerState = PlayerTTState.paused;
        _streamController.add(_player);
      });

      _player.audioPlayer.setContinueHandler(() {
        print("Continued");
        _player.playerState = PlayerTTState.continued;
        _streamController.add(_player);
      });

      _player.audioPlayer.setProgressHandler(
          (String text, int startOffset, int endOffset, String word) {
        _player.word = word;
        _streamController.add(_player);
      });
    }

    _player.audioPlayer.setErrorHandler((msg) {
      print("error: $msg");
      _player.playerState = PlayerTTState.stopped;
      _streamController.add(_player);
    });
    _streamController.add(_player);
  }

  String sanitizeText(String text) {
    text.replaceAll(RegExp('<speak>'), '');
    text.replaceAll(RegExp('</speak>'), '');
    return '<speak>' + text + '</speak>';
  }

  Future<int> play() async {
    // context.read<AnalyticsService>().logCustomEvent(name: "playButtonPressed");
    // double volume = 0.8;
    double rate = 1.0;
    double pitch = 1.0;

    // await _player.audioPlayer.setVolume(volume);
    // await _player.audioPlayer.setSpeechRate(rate);
    // await _player.audioPlayer.setPitch(pitch);

    print('Play button: ' + _player.text);

    if (_player.text != null) {
      print('Aint null');
      if (_player.text.isNotEmpty) {
        print('Aint empty');
        _player.playerState = PlayerTTState.playing;
        await _player.audioPlayer.awaitSpeakCompletion(true);
        _streamController.add(_player);
        String textToPlay = sanitizeText(_player.text);
        await _player.audioPlayer.speak(textToPlay).whenComplete(() {
          _player.playerState = PlayerTTState.stopped;
          _streamController.add(_player);
        });
      }
    }

    // final playPosition = (_player.position != null &&
    //     _player.duration != null &&
    //     _player.position.inMilliseconds > 0 &&
    //     _player.position.inMilliseconds < _player.duration.inMilliseconds)
    //     ? _player.position
    //     : Duration(milliseconds: 0);
    // print('Audio path ${_player.audioPath}');
    // final result = await _player.audioPlayer
    //     .play(_player.audioPath, position: playPosition);
    // print('Resultado $result');
    // if (result == 1) {
    //   _player.playerState = PlayerState.playing;
    //   _streamController.add(_player);
    // }
    //
    // // default playback rate is 1.0
    // // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // // this can also be called everytime the user wants to change playback rate in the UI
    // _player.audioPlayer.setPlaybackRate(playbackRate: 1.0);

    // return result;
  }

  Future<int> pause() async {
    // context.read<AnalyticsService>().logCustomEvent(name: "pauseButtonPressed");

    var result = await _player.audioPlayer.pause();
    if (result == 1) {
      _player.playerState = PlayerTTState.paused;
      _streamController.add(_player);
    }
    // final result = await _player.audioPlayer.pause();
    // if (result == 1) {
    //   _player.playerState = PlayerState.paused;
    //   _streamController.add(_player);
    // }
    // return result;
  }

  Future<int> stop() async {
    // context.read<AnalyticsService>().logCustomEvent(name: "pauseButtonPressed");

    var result = await _player.audioPlayer.stop();
    if (result == 1) {
      _player.playerState = PlayerTTState.stopped;
      _streamController.add(_player);
    }
  }

  Future<void> onComplete() async {
    var result = await _player.audioPlayer.stop();
    if (result == 1) {
      _player.playerState = PlayerTTState.stopped;
      _streamController.add(_player);
    }
    // final result = await _player.audioPlayer.stop();
    // if (result == 1) {
    //   _player.playerState = PlayerState.stopped;
    //   _player.duration = Duration(seconds: 0);
    //   _player.position = Duration(seconds: 0);
    //   _streamController.close();
    // }
  }
}
