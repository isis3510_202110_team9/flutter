import 'dart:async';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:gradua_app/model/player_model.dart';
import 'package:gradua_app/services/content_provider.dart';
import 'package:gradua_app/services/db_manager.dart';
import 'package:gradua_app/services/hive_manager.dart';

class PlayerViewModel {
  PlayerModel _player;
  // ProfilePreferences preferences = ProfilePreferences();
  HiveManager hive = HiveManager.getInstance();

  ContentProvider cp = new ContentProvider();

  StreamController<PlayerModel> _streamController =
      StreamController.broadcast();

  PlayerViewModel(String audioPath) {
    this._player = PlayerModel(audioPath: audioPath
        // '/data/user/0/com.gradua.gradua/app_flutter/Concentration'
        );
    // this._streamController.add(this.player);
    _initAudioPlayer();
    // preferences.initDB();
  }

  Stream<PlayerModel> get playerStream => _streamController.stream;

  double get fileSize {
    //Devuelve en MB el tamaño del archivo
    if (this._player.audioPath == null) return 0.0;

    File file = File(this._player.audioPath);
    bool fileExists = file.existsSync();
    double fileSize = 0.0;
    if (fileExists) {
      int bytes = file.lengthSync();

      if (bytes > 0) {
        fileSize =
            bytes / (8 * 1024 * 1024); // Convierte bytes (b) a Mega Bytes (MB)
      }
    }
    return fileSize;
  }

  void _initAudioPlayer() {
    _player.audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);

    _player.audioPlayer.onDurationChanged.listen(
      (duration) {
        _player.duration = duration;
        _streamController.add(_player);
      },
    );

    _player.audioPlayer.onAudioPositionChanged.listen(
      (p) {
        _player.position = p;
        _streamController.add(_player);
      },
    );

    _player.audioPlayer.onPlayerCompletion.listen(
      (event) {
        //  TODO oncomplete
        _player.position = _player.duration;
        _streamController.add(_player);
      },
    );

    _player.audioPlayer.onPlayerError.listen(
      (msg) {
        print('audioPlayer error : $msg');
        _player.playerState = PlayerState.stopped;
        _player.duration = Duration(seconds: 0);
        _player.position = Duration(seconds: 0);
        _streamController.add(_player);
      },
    );

    _player.audioPlayer.onPlayerCommand.listen(
      (command) {
        print('Audio player command: ');
        print(command.toString());
      },
    );

    _player.audioPlayer.onPlayerStateChanged.listen(
      (state) {
        _player.audioPlayerState = state;
        _streamController.add(_player);
      },
    );

    _player.audioPlayer.onNotificationPlayerStateChanged.listen(
      (state) {
        _player.audioPlayerState = state;
        _streamController.add(_player);
      },
    );
    //
    // _playingRouteState = PlayingRouteState.speakers;
  }

  Future<int> play() async {
    // context.read<AnalyticsService>().logCustomEvent(name: "playButtonPressed");
    final playPosition = (_player.position != null &&
            _player.duration != null &&
            _player.position.inMilliseconds > 0 &&
            _player.position.inMilliseconds < _player.duration.inMilliseconds)
        ? _player.position
        : Duration(milliseconds: 0);
    print('Audio path ${_player.audioPath}');
    final result = await _player.audioPlayer
        .play(_player.audioPath, position: playPosition);
    print('Resultado $result');
    if (result == 1) {
      _player.playerState = PlayerState.playing;
      _streamController.add(_player);
    }

    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
    _player.audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  Future<int> pause() async {
    // context.read<AnalyticsService>().logCustomEvent(name: "pauseButtonPressed");
    final result = await _player.audioPlayer.pause();
    if (result == 1) {
      _player.playerState = PlayerState.paused;
      _streamController.add(_player);
    }
    return result;
  }

  Future<void> onComplete() async {
    final result = await _player.audioPlayer.stop();
    if (result == 1) {
      _player.playerState = PlayerState.stopped;
      _player.duration = Duration(seconds: 0);
      _player.position = Duration(seconds: 0);
      _streamController.close();
    }
  }

  Future<void> addToFavorites(uuid, meditation, url) async {
    var db = new DbManager();
    await db.addMeditationToFavorites(uuid, meditation);
    hive.insertFavorite(meditation);

    //
    // ContentProvider cp = new ContentProvider();
    // String filePath = await cp.downloadFile(url, meditation);
    // print('Prior preferences before adding');
    // print(hive.getAllPreferences().toMap().toString());
    // hive.insertRoute(meditation, filePath);

    //  Descargar y guardar local

    //  Meter en box la url
  }

  Future<void> removeFromFavorites(uuid, meditation) async {
    //TODO Singletion db
    var db = new DbManager();
    await db.removeMeditationFromFavorites(uuid, meditation);
    hive.deleteFavorite(meditation);
    // ContentProvider cp = new ContentProvider();
    // cp.removeFileFromAppDocuments(meditation);
    // print('Prior preferences before removing');
    // print(hive.getAllPreferences().toMap().toString());
    // hive.removeRoute(meditation);
  }

  Future<Map<dynamic, dynamic>> downloadMeditation(meditation, url) async {
    try {
      String filePath = await cp.downloadFile(url, meditation);
      print('File Downloaded');
      print('Prior preferences before adding');
      print(hive.getAllPreferences().toMap().toString());
      hive.insertRoute(meditation, filePath);
      return {
        'success': true,
        'path': filePath,
      };
    } catch (e) {
      print('Error while loading meditation');
      print(e.toString());
      return {
        'success': false,
        'path': '',
      };
    }
  }

  Future<bool> deleteDownloadedMeditation(meditation) async {
    try {
      await cp.removeFileFromAppDocuments(meditation);
      print('Prior preferences before removing');
      print(hive.getAllPreferences().toMap().toString());
      hive.removeRoute(meditation);
      return true;
    } catch (e) {
      print('Error eliminando archivo local');
      print(e.toString());
      return false;
    }
  }
}
